package controllers

import (
	"net/http"

	"github.com/dchest/captcha"

	"gitlab.com/tsauter/segexch/config"
)

// NewCaptchas returns a configured Captchas controller. The controller contains the
// everything to serve Captchas.
// The Captchas are stored in an in-memory database.
func NewCaptchas(config config.Config) *Captchas {
	return &Captchas{
		Config: config,
	}
}

// Captchas is the controller part of the MCV model.
// The controller is sitting between the models and the views.
type Captchas struct {
	// Config is the global configuration
	Config config.Config
}

// CaptchaServer returns an http.Handler that can be used in mux router
// to server image and audio captchas.
func (c *Captchas) CaptchaServer() http.Handler {
	return captcha.Server(captcha.StdWidth, captcha.StdHeight)
}
