package controllers

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"path"
	"time"

	"github.com/dchest/captcha"
	"github.com/gorilla/csrf"
	"github.com/gorilla/mux"

	"gitlab.com/tsauter/segexch/config"
	"gitlab.com/tsauter/segexch/models"
	"gitlab.com/tsauter/segexch/views"
)

// NewSecrets returns a configured Secrets controller. The controller contains the
// configuration, all necessary views and the pointer to the backend server, used
// for handling the Secrets itself.
func NewSecrets(config config.Config, secretService models.SecretService) *Secrets {
	return &Secrets{
		Config:          config,
		IndexView:       views.NewView(config.HTMLLayout, "secrets/index"),
		IndexStoredView: views.NewView(config.HTMLLayout, "secrets/index_stored"),
		LandingView:     views.NewView(config.HTMLLayout, "secrets/landing"),
		DetailView:      views.NewView(config.HTMLLayout, "secrets/detail"),
		SecretService:   secretService,
	}
}

// Secrets is the controller part of the MCV model.
// The controller is sitting between the models and the views.
type Secrets struct {
	// Config is the global configuration
	Config config.Config
	// IndexView is the view for the Index page
	IndexView views.ViewRenderer
	// IndexStoredView is the view for the Index page, when the data was stored
	IndexStoredView views.ViewRenderer
	// LandingView is the view for the Landing page
	LandingView views.ViewRenderer
	// DetailView is the view for the Detail page
	DetailView views.ViewRenderer
	// models.SecretService is the pointer to the backend database
	models.SecretService
}

// A CreateForm contains all data submitted to the Create page.
// Either by *direct* POST request, or by POST request through the
// web form.
// These data contains insecure and untrusted information.
type CreateForm struct {
	// Secret is the plain unencrypted representation of the magic to store
	Secret string
	// Password is the optional password to encrypt the magic (makes the secret unencryptable by the server admins)
	Password string
	// csrfField contains the csrf token
	CsrfField string
	// UseCaptcha is used to configure the use of an additional captcha protection layer
	UseCaptcha bool
	// CaptchaID is used to pass the generated Captcha ID to the form the fetch the image
	CaptchaID string
	// Captcha holds the entered captcha text, which muse be evaluated
	Captcha string
}

// A DetailForm contains all data submitted to the Detail page.
// Either by *direct* POST request, or by POST request through the
// web form.
// These data contains insecure and untrusted information.
type DetailForm struct {
	// MagicEncrypted is the encrypted representation of the magic id
	MagicEncrypted string
	// Password is the password to encrypt the magic
	Password string
}

// A SecretResponse represents the result data
// that is send back as an answer to an POST request to create
// a new Secret.
type SecretResponse struct {
	// URL is the full URL to access to Secret
	URL string `json:"URL"`
	// ExpiryAt is the date until the Secret is valid and accessible
	ExpiryAt time.Time
}

// RedirectStartpage redirects to the default page (this is required for the
// CSFR protection.
// The redirect url is build from config's BaseURL.
func (s *Secrets) RedirectStartpage(w http.ResponseWriter, r *http.Request) {
	url, err := url.Parse(s.Config.BaseURL)
	if err != nil {
		log.Printf("failed to parse BaseURL from config: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	url.Path = path.Join(url.Path, "/secret/")

	http.Redirect(w, r, url.String()+"/", http.StatusMovedPermanently)
}

// Index returns the default page to store a new Secret.
func (s *Secrets) Index(w http.ResponseWriter, r *http.Request) {
	// We accept GET query parameters to pass the secret
	// When the parsing process returns an error just empty data is used.
	var requestData CreateForm
	parseQuery(r, &requestData)

	// passwd secret must be url encoded to allow all special characters (javascript: encodeURIComponent())
	secretDecoded, err := url.QueryUnescape(requestData.Secret)
	if err != nil {
		log.Printf("failed to convert passed secret: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	requestData.Secret = secretDecoded

	yielddata := CreateForm{
		Secret:     requestData.Secret,
		CsrfField:  csrf.Token(r),
		UseCaptcha: s.Config.UseCaptcha,
		CaptchaID:  captcha.New(),
	}
	data := views.Data{Yield: yielddata}
	s.IndexView.Render(w, data)
}

// Create is called when a POST request from the the Index page is received to
// store a new Secret (Create from CRUD).
// It takes the sent form data and forwards them to the storage backend.
func (s *Secrets) Create(w http.ResponseWriter, r *http.Request) {
	// Try to parse the form POST to an CreateForm structure.
	var requestData CreateForm
	err := parseForm(r, &requestData)
	requestData.UseCaptcha = s.Config.UseCaptcha // overwrite the use of captcha
	if err != nil {
		log.Printf("invalid data received: %v", err)
		requestData.CaptchaID = captcha.New() // reset captcha id to a new one
		vd := views.Data{Yield: requestData}
		vd.AddAlerts(views.AlertError("Invalid data received."))
		s.IndexView.RenderError(w, vd)
		return
	}

	// Make sure we have a passed text at all, it make no sense to continue otherwise
	if requestData.Secret == "" {
		log.Printf("invalid data received: %v", err)
		requestData.CaptchaID = captcha.New() // reset captcha id to a new one
		vd := views.Data{Yield: requestData}
		vd.AddAlerts(views.AlertError("At least the Secret is required to continue."))
		s.IndexView.RenderError(w, vd)
		return
	}

	// Validate the passed in Captcha text
	if s.Config.UseCaptcha {
		if !captcha.VerifyString(requestData.CaptchaID, requestData.Captcha) {
			log.Printf("invalid captcha received")
			requestData.CaptchaID = captcha.New() // reset captcha id to a new one
			vd := views.Data{Yield: requestData}
			vd.AddAlerts(views.AlertError("Captcha is invalid."))
			s.IndexView.RenderError(w, vd)
			return
		}
	}

	// Create secret with all available data passed in through the POST request.
	// The data is still INSECURE, UNTRUSTED and UNVALIDATED.
	secret := &models.Secret{SecretText: requestData.Secret}

	// Hand over the temporary insecure secret to the backend, it will
	// take care of all further steps (validating, storing, etc.)
	// After that, secret is VALID and can be used for further actions.
	err = s.SecretService.Create(secret)
	if err != nil {
		log.Printf("failed to store new secret: %v\n", err)
		requestData.CaptchaID = captcha.New() // reset captcha id to a new one
		vd := views.Data{Yield: requestData}
		vd.AddAlerts(views.AlertGeneric)
		s.IndexView.RenderError(w, vd)
		return
	}

	// The passed-back data to the user contains a URL link to fetch the stored secret.
	// We build this link here; the base portion of the url can be different on various
	// deployments.
	// The link also contains the password as GET parameter (?password=xxxx).
	// The purpose *is* to share the passwords with others, so this isn't a security problem.
	// e.g.
	//  - http://127.0.0.1:8080/secret/xxxxxxxxxxxxxxxxx?password=yyyy
	//  - http://myhostname.xxx.com/segexch/secret/xxxxxxxxxxxxxxxxx?password=yyyyh
	url, err := buildSecretDownloadURL(s.Config.BaseURL, *secret)
	if err != nil {
		log.Printf("failed to build download url: %v\n", err)
		requestData.CaptchaID = captcha.New() // reset captcha id to a new one
		vd := views.Data{Yield: requestData}
		vd.AddAlerts(views.AlertGeneric)
		s.IndexView.RenderError(w, vd)
		return
	}

	log.Printf("stored new secret: %d (expire at %s)", secret.ID, secret.ExpiryAt)

	// create a response and send the data back (json format)
	// sendJSON() is response to encode the data correctly
	// Magic is already URL encoded, the password not, but we keep it to have the
	// real password for the user.
	secretdata := SecretResponse{
		URL:      url,
		ExpiryAt: secret.ExpiryAt,
	}
	response := views.Data{Yield: secretdata}
	s.IndexStoredView.Render(w, response)
}

// Landing is called as an intermediate page before the user
// really fetch the secret from database. This page should be used as the shared link.
// The generated HTML page simply redirect to the final detail page.
// All data is still encrypted (not fetched) at this moment.
func (s *Secrets) Landing(w http.ResponseWriter, r *http.Request) {
	// get the extracted magic from; handle by the mux package HandlerFunc() (regex)
	// render an error page if the magic is not present
	// magic is INSECURE and UNTRUSTED here
	vars := mux.Vars(r)
	magic, ok := vars["magic"]
	if !ok {
		var vd views.Data
		vd.AddAlerts(views.AlertMagicMissing)
		s.DetailView.RenderError(w, vd)
		return
	}

	// unmarshal the POST data to an DetailForm{} struct
	// we try to get the password for the secret here
	// render an error page in case of an error
	// magic, password (and other data) are INSECURE and UNTRUSTED here
	data := &DetailForm{MagicEncrypted: magic}
	parseQuery(r, data)
	if data.Password == "" {
		var vd views.Data
		vd.AddAlerts(views.AlertMagicMissing)
		s.DetailView.RenderError(w, vd)
		return
	}

	// pass the retured secret to the views.Data{} struct and render the template
	// the Yield field is used for generic data, the Error field for errors
	// we expect trusted and secured data from the backend
	s.LandingView.Render(w, views.Data{Yield: data})
}

// Detail is called the load the secret from the database, and
// display the secret to the user; or display an error message.
// The magic id of the secret is part of the URL and will be extracted
// by the mux package out of the URL portion. The should already made,
// otherwise the function shouldn't have been called.
// The function must be invoked through an HTTP POST request.
// The required password is extracted from the POST data.
func (s *Secrets) Detail(w http.ResponseWriter, r *http.Request) {
	// get the extracted magic from; handle by the mux package HandlerFunc() (regex)
	// render an error page if the magic is not present
	// magic is INSECURE and UNTRUSTED here
	vars := mux.Vars(r)
	magic, ok := vars["magic"]
	if !ok {
		var vd views.Data
		vd.AddAlerts(views.AlertMagicMissing)
		s.DetailView.RenderError(w, vd)
		return
	}

	// unmarshal the POST data to an DetailForm{} struct
	// we try to get the password for the secret here
	// render an error page in case of an error
	// magic, password (and other data) are INSECURE and UNTRUSTED here
	data := &DetailForm{MagicEncrypted: magic}
	parseQuery(r, data)
	if data.Password == "" {
		var vd views.Data
		vd.AddAlerts(views.AlertMagicMissing)
		s.DetailView.RenderError(w, vd)
		return
	}

	// query the backend database for the requested service
	// again, error page if something is wrong, or the secret couln't found
	// the passed data are INSECURE and UNTRUSTED
	secret := s.SecretService.ByMagic(
		models.Secret{MagicEncrypted: data.MagicEncrypted, Password: data.Password},
	)
	if secret == nil {
		var vd views.Data
		vd.AddAlerts(views.AlertError("Unknown Secret or the Secret is already expired!"))
		s.DetailView.RenderError(w, vd)
		return
	}

	log.Printf("fetched secret: %d (magic %s)", secret.ID, secret.MagicUUID)

	// pass the retured secret to the views.Data{} struct and render the template
	// the Yield field is used for generic data, the Error field for errors
	// we expect trusted and secured data from the backend
	s.DetailView.Render(w, views.Data{Yield: secret})
}

// An APISecretFetchRequest represents the incoming data of the
// API request to fetch the existing Secret.
type APISecretFetchRequest struct {
	// MagicEncrypted is the unique identified to access the secret
	MagicEncrypted string `schema:"Magic"`
	// Passwort is the password, without the password, the Magic isn't accessible
	Password string `schema:"Password"`
}

// An APISecretFetchResponse represents the result data
// that are send back as an answert to an API request to fetch
// an existing Secret.
type APISecretFetchResponse struct {
	// Secret is the text that should be stored
	Secret string `json:"Secret"`
	// SecretBase64 is the text that should be stored
	SecretBase64 string `json:"SecretBase64"`
}

// APIRead is called for the API endpoint to retrieve an existing secret. (Read from CRUD)
// It takes the sent JSON data and forwards them to the storage backend.
func (s *Secrets) APIRead(w http.ResponseWriter, r *http.Request) {
	// Try to parse the GET query parameters to an APISecretFetchRequest structure.
	var requestData APISecretFetchRequest
	err := parseQuery(r, &requestData)
	if err != nil {
		http.Error(w, "invalid GET data", http.StatusBadRequest)
		return
	}

	// Make sure we have a passed magic at all, it make no sense to continue otherwise
	if requestData.MagicEncrypted == "" {
		http.Error(w, "invalid GET data", http.StatusBadRequest)
		return
	}

	// Make sure we have a passed password at all, it make no sense to continue otherwise
	if requestData.Password == "" {
		http.Error(w, "invalid GET data", http.StatusBadRequest)
		return
	}

	// query the backend database for the requested service
	// again, error page if something is wrong, or the secret couln't found
	// the passed data are INSECURE and UNTRUSTED
	secret := s.SecretService.ByMagic(
		models.Secret{MagicEncrypted: requestData.MagicEncrypted, Password: requestData.Password},
	)
	if secret == nil {
		http.Error(w, "unknown secret or the secret is already expired", http.StatusForbidden)
		return
	}

	base64secret := base64.StdEncoding.EncodeToString([]byte(secret.SecretText))

	log.Printf("fetched secret: %d (magic %s)", secret.ID, secret.MagicUUID)

	// create a response and send the data back (json format)
	// sendJSON() is response to encode the data correctly
	// Magic is already URL encoded, the password not, but we keep it to have the
	// real password for the user.
	response := APISecretFetchResponse{
		Secret:       secret.SecretText,
		SecretBase64: base64secret,
	}
	if err := sendJSON(w, &response); err != nil {
		log.Printf("failed to send JSON result: %v", err)
		return
	}
}

// An APISecretStoreRequest represents the incoming data of the
// API request to create new Secrets.
type APISecretStoreRequest struct {
	// APIKey is the "password" for the API request
	APIKey string `json:"APIKey"`
	// Secret is the text that should be stored
	Secret string `json:"Secret"`
}

// An APISecretStoreResponse represents the result data
// that are send back as an answert to an API request to create
// a new Secret.
type APISecretStoreResponse struct {
	// Magic is the unique identified to access the secret
	Magic string `json:"Magic"`
	// Passwort is the password, without the password, the Magic isn't accessible
	Password string `json:"Password"`
	// PasswortEncoded is the URL encoded password, without the password, the Magic isn't accessible
	PasswordEncoded string `json:"PasswordEncoded"`
	// URL is the full URL to access to Secret
	URL string `json:"URL"`
	// ExpiryAt is the date until the Secret is valid and accessible
	ExpiryAt time.Time
}

// APICreate is called for the API endpoint to store a new secret. (Create from CRUD)
// It takes the sent JSON data and forwards them to the storage backend.
func (s *Secrets) APICreate(w http.ResponseWriter, r *http.Request) {
	// Try to parse the JSON POST body to an APISecretStoreRequest structure.
	var requestData APISecretStoreRequest
	err := parseJSON(r, &requestData)
	if err != nil {
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}

	// Make sure we have a passed text at all, it make no sense to continue otherwise
	if requestData.Secret == "" {
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}

	// Validate the used API key, if the key doesn't match, we can abort.
	if (requestData.APIKey == "") || (requestData.APIKey != s.Config.APIKey) {
		http.Error(w, "authentication failed", http.StatusForbidden)
		return
	}

	// Create secret with all available data passed in through JSON.
	// The data is still INSECURE, UNTRUSTED and UNVALIDATED.
	secret := &models.Secret{SecretText: requestData.Secret}

	// Hand over the temporary insecure secret to the backend, it will
	// take care of all further steps (validating, storing, etc.)
	// After that, secret is VALID and can be used for further actions.
	err = s.SecretService.Create(secret)
	if err != nil {
		http.Error(w, "failed to store new secret", http.StatusInternalServerError)
		return
	}

	// The passed-back data to the user contains a URL link to fetch the stored secret.
	// We build this link here; the base portion of the url can be different on various
	// deployments.
	// The link also contains the password as GET parameter (?password=xxxx).
	// The purpose *is* to share the passwords with others, so this isn't a security problem.
	// e.g.
	//  - http://127.0.0.1:8080/secret/xxxxxxxxxxxxxxxxx?password=yyyy
	//  - http://myhostname.xxx.com/segexch/secret/xxxxxxxxxxxxxxxxx?password=yyyyh
	url, err := buildSecretDownloadURL(s.Config.BaseURL, *secret)
	if err != nil {
		http.Error(w, "failed to build download url", http.StatusInternalServerError)
		return
	}

	log.Printf("stored new secret: %d (expire at %s)", secret.ID, secret.ExpiryAt)

	// create a response and send the data back (json format)
	// sendJSON() is response to encode the data correctly
	// Magic is already URL encoded, the password not, but we keep it to have the
	// real password for the user.
	response := APISecretStoreResponse{
		Magic:           secret.MagicEncrypted,
		Password:        secret.Password,
		PasswordEncoded: base64.RawURLEncoding.EncodeToString([]byte(secret.Password)), // this must be URL encoded, since the fetch function requires it
		URL:             url,
		ExpiryAt:        secret.ExpiryAt,
	}
	if err := sendJSON(w, &response); err != nil {
		log.Printf("failed to send JSON result: %v", err)
		return
	}
}

// buildSecretDownloadURL creates the URL to directly access/download the secret.
// The link also contains the password as GET parameter (?password=xxxx).
// e.g.
//   - http://127.0.0.1:8080/secret/xxxxxxxxxxxxxxxxx?password=yyyy
//   - http://myhostname.xxx.com/segexch/secret/xxxxxxxxxxxxxxxxx?password=yyyyh
func buildSecretDownloadURL(baseURL string, secret models.Secret) (string, error) {
	url, err := url.Parse(baseURL)
	if err != nil {
		return "", err
	}
	// Magic is already URL encoded, the password not
	url.Path = path.Join(url.Path, "/secret")
	url.Path = path.Join(url.Path, secret.MagicEncrypted)
	url.RawQuery = fmt.Sprintf("%s=%s", "password", base64.RawURLEncoding.EncodeToString([]byte(secret.Password)))
	return url.String(), nil
}
