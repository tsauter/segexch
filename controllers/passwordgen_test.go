package controllers

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/tsauter/segexch/config"
	"gitlab.com/tsauter/segexch/views/viewmock"
)

// TestPasswordGenIndex tests the Index() function.
// We pass no real HTML template, which means we should receive a simple text
// from the viewmock.
func TestPasswordGenIndex(t *testing.T) {
	// out testcases
	testcases := []struct {
		name       string
		r          *http.Request
		statusCode int
		result     string
	}{
		{
			"case1",
			httptest.NewRequest(
				"GET",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(``)),
			200,
			"MOCK",
		},
		{
			"case2",
			httptest.NewRequest(
				"GET",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(``)),
			200,
			"MOCK",
		},
	}

	// create the PasswordGenerator controller
	pg := PasswordGenerator{
		config.Config{},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}{{end}}"},
	}

	// Simulate a POST request to the httptest package.
	// Compare the output of the "generated" page against our expected string.
	// The generator and the backend are both mocks that simulates the real behaviour.
	for _, tc := range testcases {
		w := httptest.NewRecorder()

		// Call the function to test, this should always return a simple html page
		pg.Index(w, tc.r)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("%s: failed to read body", tc.name)
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statusCode {
			t.Fatalf("%s: expected status code is different: %d: %s", tc.name, resp.StatusCode, strings.TrimSpace(string(body)))
		}

		bodyStr := strings.TrimSpace(string(body))

		if bodyStr != tc.result {
			t.Fatalf("%s: expected body is different: \"%s\"", tc.name, bodyStr)
		}
	}
}

// TestPasswordWebGenAPICreate tests the WebAPICreate() function.
func TestPasswordGenWebAPICreate(t *testing.T) {
	// out testcases
	testcases := []struct {
		name       string
		r          *http.Request
		statusCode int
		result     string
	}{
		{
			// no text passed
			"case1",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(`{}`)),
			400,
			`invalid JSON data`,
		},
		{
			// pass in an invalid ID
			"case2",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(`{"scheme_id": "99"}`)),
			400,
			`invalid JSON data`,
		},
		{
			// pass in an valid ID
			"case3",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(`{"scheme_id": "0"}`)),
			200,
			`{"password":"","used_pattern":""}`,
		},
	}

	// create a Config object with the required settings
	c := config.Config{
		BaseURL:                "http://localhost:8080",
		PasswordSchemeOrdering: []string{"scheme1", "scheme2"},
		PasswordSchemes: config.PasswordSchemeList{
			"scheme1": config.PasswordScheme{},
			"scheme2": config.PasswordScheme{},
		},
	}

	// create the PasswordGenerator controller
	pg := &PasswordGenerator{
		c,
		&viewmock.ViewMock{},
	}

	// Simulate a POST request to the httptest package.
	// Compare the HTTP status code.
	// Compare the output of the "generated" JSON response against our expected string.
	// The generator and the backend are both mocks that simulates the real behaviour.
	for _, tc := range testcases {
		w := httptest.NewRecorder()

		// call the WebAPICreate() function, this is the function we want to test
		pg.WebAPICreate(w, tc.r)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("%s: failed to read body", tc.name)
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statusCode {
			t.Fatalf("%s: expected status code is different: %d: %s", tc.name, resp.StatusCode, strings.TrimSpace(string(body)))
		}

		bodyStr := strings.TrimSpace(string(body))

		if bodyStr != tc.result {
			t.Fatalf("%s: expected body is different: \"%s\"", tc.name, bodyStr)
		}
	}
}

// TestPasswordGenAPICreate tests the APICreate() function.
func TestPasswordGenAPICreate(t *testing.T) {
	// out testcases
	testcases := []struct {
		name       string
		r          *http.Request
		statusCode int
		result     string
	}{
		{
			// no text passed
			"case1",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(`{}`)),
			403,
			`authentication failed`,
		},
		{
			// pass in an invalid ID
			"case2",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(`{"scheme_id": "99", "APIKey": "12345"}`)),
			400,
			`invalid JSON data`,
		},
		{
			// pass in an valid ID
			"case3",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/passwordgen/",
				strings.NewReader(`{"scheme_id": "0", "APIKey": "12345"}`)),
			200,
			`{"password":"","password_base64":"","used_pattern":""}`,
		},
	}

	// create a Config object with the required settings
	c := config.Config{
		BaseURL:                "http://localhost:8080",
		PasswordSchemeOrdering: []string{"scheme1", "scheme2"},
		PasswordSchemes: config.PasswordSchemeList{
			"scheme1": config.PasswordScheme{},
			"scheme2": config.PasswordScheme{},
		},
		APIKey: "12345",
	}

	// create the PasswordGenerator controller
	pg := &PasswordGenerator{
		c,
		&viewmock.ViewMock{},
	}

	// Simulate a POST request to the httptest package.
	// Compare the HTTP status code.
	// Compare the output of the "generated" JSON response against our expected string.
	// The generator and the backend are both mocks that simulates the real behaviour.
	for _, tc := range testcases {
		w := httptest.NewRecorder()

		// call the WebAPICreate() function, this is the function we want to test
		pg.APICreate(w, tc.r)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("%s: failed to read body", tc.name)
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statusCode {
			t.Fatalf("%s: expected status code is different: %d: %s", tc.name, resp.StatusCode, strings.TrimSpace(string(body)))
		}

		bodyStr := strings.TrimSpace(string(body))

		if bodyStr != tc.result {
			t.Fatalf("%s: expected body is different: \"%s\"", tc.name, bodyStr)
		}
	}
}
