package controllers

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"

	"github.com/gorilla/mux"

	"gitlab.com/tsauter/segexch/config"
	"gitlab.com/tsauter/segexch/models"
	"gitlab.com/tsauter/segexch/models/modelmock"
	"gitlab.com/tsauter/segexch/views/viewmock"
)

// TestIndex tests the Index() function.
// We pass no real HTML template, which means we should receive a simple text
// from the viewmock.
func TestIndex(t *testing.T) {
	// out testcases
	testcases := []struct {
		name       string
		r          *http.Request
		statusCode int
		result     string
	}{
		{
			"case1",
			httptest.NewRequest(
				"GET",
				"http://localhost:8080/secret/",
				strings.NewReader(``)),
			200,
			"MOCK",
		},
		{
			"case2",
			httptest.NewRequest(
				"GET",
				"http://localhost:8080/secret/",
				strings.NewReader(``)),
			200,
			"MOCK",
		},
	}

	// create the Secrets controller
	s := Secrets{
		config.Config{},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}{{end}}"},
		&modelmock.SecretServiceMock{},
	}

	// Simulate a POST request to the httptest package.
	// Compare the output of the "generated" page against our expected string.
	// The generator and the backend are both mocks that simulates the real behaviour.
	for _, tc := range testcases {
		w := httptest.NewRecorder()

		// Call the function to test, this should always return a simple html page
		s.Index(w, tc.r)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("%s: failed to read body", tc.name)
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statusCode {
			t.Fatalf("%s: expected status code is different: %d: %s", tc.name, resp.StatusCode, strings.TrimSpace(string(body)))
		}

		bodyStr := strings.TrimSpace(string(body))

		if bodyStr != tc.result {
			t.Fatalf("%s: expected body is different: \"%s\"", tc.name, bodyStr)
		}
	}
}

// TestCreate tests the Create() function.
func TestCreate(t *testing.T) {
	// out testcases
	testcases := []struct {
		name     string
		url      string
		formData map[string]string
		result   string
	}{
		{
			// no secret passed
			"case1",
			"http://localhost:8080/secret/",
			map[string]string{},
			`danger/At least the Secret is required to continue.;`,
		},
		{
			// invalid fields passed
			"case2",
			"http://localhost:8080/secret/",
			map[string]string{"no-secret-passed": "test", "Secret": "test"},
			`danger/Invalid data received.;`,
		},
		{
			// secret passed
			"case3",
			"http://localhost:8080/secret/",
			map[string]string{"Secret": "test"},
			`MOCK/OK`,
		},
	}

	// create the Secrets controller
	// we cannot pass back the URL because this string will change every runtime!
	s := Secrets{
		config.Config{UseCaptcha: false},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/OK{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/OK{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/OK{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/OK{{end}}"},
		&modelmock.SecretServiceMock{},
	}

	// Simulate a POST request to the httptest package.
	// Compare the output of the "generated" page against our expected string.
	// The generator and the backend are both mocks that simulates the real behaviour.
	for _, tc := range testcases {
		w := httptest.NewRecorder()

		// convert the map of parameters to URL parameters
		data := url.Values{}
		for k, v := range tc.formData {
			data.Add(k, v)
		}

		// create a dummy request to send form data via fake-POST-request
		req := httptest.NewRequest("POST", tc.url, strings.NewReader(data.Encode()))
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))

		// Call the function to test, this should always return a simple html page
		s.Create(w, req)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("%s: failed to read body", tc.name)
		}
		resp.Body.Close()

		bodyStr := strings.TrimSpace(string(body))

		if bodyStr != tc.result {
			t.Fatalf("%s: expected body is different: \"%s\"", tc.name, bodyStr)
		}
	}
}

// TestDetail tests the Detail() function.
func TestDetail(t *testing.T) {
	// out testcases
	testcases := []struct {
		name    string
		r       *http.Request
		muxvars map[string]string
		result  string
	}{
		{
			// URL without a secret -> error page
			"case1",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/secret/",
				strings.NewReader(`{}`)),
			map[string]string{},
			`danger/Oops! Not sure what you're looking for...;`,
		},
		{
			// URL with secret, but no password in POST data -> error page
			"case2",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/secret/myinvalidmagic",
				strings.NewReader(`{}`)),
			map[string]string{"magic": "myinvalidmagic"},
			`danger/Oops! Not sure what you're looking for...;`,
		},
		{
			// URL with secret and password; not available in the backend -> error page: unknown secret
			"case3",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/secret/myinvalidmagic?password=myinvalidpass",
				strings.NewReader(`{"password": "mypinvalidass"}`)),
			map[string]string{"magic": "myinvalidmagic"},
			`danger/Unknown Secret or the Secret is already expired!;`,
		},
		{
			// URL with secret and password; available in the backend -> page with secret
			"case4",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/secret/secret1?password=mypass",
				strings.NewReader(`{"password": "mypass"}`)),
			map[string]string{"magic": "secret1"},
			`MOCK/76a27846-bca4-4336-b677-8c5febbb1943/mytext1`,
		},
	}

	// create the Secrets controller
	s := Secrets{
		config.Config{},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/{{.MagicUUID}}/{{.SecretText}}{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/{{.MagicUUID}}/{{.SecretText}}{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/{{.MagicUUID}}/{{.SecretText}}{{end}}"},
		&viewmock.ViewMock{TemplateString: "MOCK{{with .Yield}}/{{.MagicUUID}}/{{.SecretText}}{{end}}"},
		&modelmock.SecretServiceMock{},
	}

	// Simulate a POST request to the httptest package.
	// The SetURLVars emulates the mux.Router regular expression extraction of
	// the url parts: r.HandleFunc("/secret/{magic:[A-Za-z0-9_-]+}", secretsC.Detail).
	// Compare the output of the "generated" page against our expected string.
	// The generator and the backend are both mocks that simulates the real behaviour.
	for _, tc := range testcases {
		w := httptest.NewRecorder()

		// set the mux vars from "router"
		tc.r = mux.SetURLVars(tc.r, tc.muxvars)

		s.Detail(w, tc.r)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("%s: failed to read body", tc.name)
		}
		resp.Body.Close()

		bodyStr := strings.TrimSpace(string(body))

		if bodyStr != tc.result {
			t.Fatalf("%s: expected body is different: \"%s\"", tc.name, bodyStr)
		}
	}
}

// TestAPICreate tests the APICreate() function.
func TestAPICreate(t *testing.T) {
	// out testcases
	testcases := []struct {
		name       string
		r          *http.Request
		statusCode int
		result     string
	}{
		{
			// no text passed
			"case1",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/segexch/api/v1/secret",
				strings.NewReader(`{}`)),
			400,
			`invalid JSON data`,
		},
		{
			// text passed, invalid API authentication key
			"case2",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/segexch/api/v1/secret",
				strings.NewReader(`{"apikey": "invalidapikey", "secret": "mytext"}`)),
			403,
			`authentication failed`,
		},
		{
			// text passed and valid API key
			"case3",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/segexch/api/v1/secret",
				strings.NewReader(`{"apikey": "validkey", "secret": "mytext"}`)),
			200,
			`{"Magic":"76a27846-bca4-4336-b677-8c5febbb1943","Password":"12345","URL":"http://localhost:8080/secret/76a27846-bca4-4336-b677-8c5febbb1943?password=MTIzNDU","ExpiryAt":"0001-01-01T00:00:00Z"}`,
		},
		{
			// text passed and valid API key, II
			"case4",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/segexch/api/v1/secret",
				strings.NewReader(`{"apikey": "validkey", "secret": "mytext"}`)),
			200,
			`{"Magic":"76a27846-bca4-4336-b677-8c5febbb1943","Password":"12345","URL":"http://localhost:8080/secret/76a27846-bca4-4336-b677-8c5febbb1943?password=MTIzNDU","ExpiryAt":"0001-01-01T00:00:00Z"}`,
		},
	}

	// create a Config object with the required settings
	c := config.Config{
		BaseURL:    "http://localhost:8080",
		APIKey:     "validkey",
		UseCaptcha: false,
	}

	// create the Secrets controller
	s := &Secrets{
		c,
		&viewmock.ViewMock{},
		&viewmock.ViewMock{},
		&viewmock.ViewMock{},
		&viewmock.ViewMock{},
		&modelmock.SecretServiceMock{},
	}

	// Simulate a POST request to the httptest package.
	// Compare the HTTP status code.
	// Compare the output of the "generated" JSON response against our expected string.
	// The generator and the backend are both mocks that simulates the real behaviour.
	for _, tc := range testcases {
		w := httptest.NewRecorder()

		// call the APICreate() function, this is the function we want to test
		s.APICreate(w, tc.r)

		resp := w.Result()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("%s: failed to read body", tc.name)
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statusCode {
			t.Fatalf("%s: expected status code is different: %d: %s", tc.name, resp.StatusCode, strings.TrimSpace(string(body)))
		}

		bodyStr := strings.TrimSpace(string(body))

		if bodyStr != tc.result {
			t.Fatalf("%s: expected body is different: \"%s\"", tc.name, bodyStr)
		}
	}
}

// TestBuildSecretDownloadURL tests te buildSecretDownloadURL() function
func TestBuildSecretDownloadURL(t *testing.T) {
	// out testcases
	testcases := []struct {
		name    string
		baseURL string
		secret  models.Secret
		result  string
	}{
		{
			"case1",
			"http://localhost:8080",
			models.Secret{
				MagicEncrypted: "mymagic",
				Password:       "mypass",
			},
			"http://localhost:8080/secret/mymagic?password=bXlwYXNz",
		},
		{
			"case2",
			"https://www.example.com",
			models.Secret{
				MagicEncrypted: "mymagic",
				Password:       "mypass",
			},
			"https://www.example.com/secret/mymagic?password=bXlwYXNz",
		},
		{
			"case3",
			"https://www.example.com/xxx",
			models.Secret{
				MagicEncrypted: "mymagic",
				Password:       "mypass",
			},
			"https://www.example.com/xxx/secret/mymagic?password=bXlwYXNz",
		},
	}

	// walk over each test cases, build the URL and compare the generated string
	for _, tc := range testcases {
		url, err := buildSecretDownloadURL(tc.baseURL, tc.secret)
		if err != nil {
			t.Fatalf("%s: URL building failed: %v", tc.name, err)
		}

		if url != tc.result {
			t.Fatalf("%s: expected URL is different: %s", tc.name, url)
		}
	}
}
