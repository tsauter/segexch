package controllers

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/csrf"
	"github.com/sethvargo/go-password/password"

	"gitlab.com/tsauter/segexch/config"
	"gitlab.com/tsauter/segexch/views"
)

const (
	DEFAULT_SCHEMAID_FOR_API = "0"
)

// NewPasswordGenerator returns a configured PasswordGenerator controller. The controller contains the
// configuration and all necessary views. An additional backend server is not used
// for this controller.
func NewPasswordGenerator(config config.Config) (*PasswordGenerator, error) {
	// validate the specified password schemes:
	// - make sure we have at least one
	// - that are all listed schemes have their configuration entry
	// - valid description
	if len(config.PasswordSchemes) < 1 {
		return nil, fmt.Errorf("no password schemes specified")
	}

	for _, k := range config.PasswordSchemeOrdering {
		scheme, err := config.PasswordSchemes.Get(strings.ToLower(k))
		if err != nil {
			return nil, fmt.Errorf("password scheme in sort list not configured: %s", k)
		}

		if scheme.Description == "" {
			return nil, fmt.Errorf("failed to parse password scheme: %s", "name is empty")
		}
	}

	return &PasswordGenerator{
		Config:    config,
		IndexView: views.NewView(config.HTMLLayout, "passwordgen/index"),
	}, nil
}

// PasswordGenerator is the controller part of the MCV model.
// The controller is sitting between the models and the views.
type PasswordGenerator struct {
	// Config is the global configuration
	Config config.Config
	// IndexView is the view for the Index page
	IndexView views.ViewRenderer
}

// A PassGenCreateForm contains all data submitted to the Create page.
// Either by *direct* POST request, or by POST request through the
// web form.
// These data contains insecure and untrusted information.
type PassGenCreateForm struct {
	// csrfField contains the csrf token
	CsrfField string
	// PasswordSchemes is a list of names of the password schemes (taken from config file)
	PasswordSchemes []string
}

// An WebAPIPasswordRequest contains all data that was submitted through
// the AJAX call to the Create() function.
// Data are send as JSON data.
type WebAPIPasswordRequest struct {
	// csrfField contains the csrf token
	CsrfField string `json:"csrfField"`
	// SchemeID contains the ID (list position) of the requested password scheme
	SchemeID string `json:"scheme_id"`
}

// An WebAPIPasswordResponse represents the result data
// that is send back as an answer to an POST request to generate
// a new password.
type WebAPIPasswordResponse struct {
	// Password contains the plain unencrypted password!
	Password string `json:"password"`
	// UsedPattern contains the regex pattern that was used for the generation
	// passing the ID eliminate the need of string validation for invalid requests
	UsedPattern string `json:"used_pattern"`
}

// An PasswordRequest contains all data that was submitted through
// the API call to the Create() function.
// Data are send as JSON data.
type APIPasswordRequest struct {
	// APIKey is the "password" for the API request
	APIKey string `json:"APIKey"`
	// SchemeID contains the ID (list position) of the requested password scheme
	SchemeID string `json:"scheme_id"`
}

// An APIPasswordResponse represents the result data for API requests
// that is send back as an answer to an POST request to generate
// a new password.
type APIPasswordResponse struct {
	// Password contains the plain unencrypted password!
	Password string `json:"password"`
	// PasswordBase64 contains the base64 encoded unencrypted password!
	PasswordBase64 string `json:"password_base64"`
	// UsedPattern contains the regex pattern that was used for the generation
	// passing the ID eliminate the need of string validation for invalid requests
	UsedPattern string `json:"used_pattern"`
}

// Index returns the default page to generate a new password.
// The password will be retrieved through an Ajax POST request to the Create() endpoint.
func (pg *PasswordGenerator) Index(w http.ResponseWriter, r *http.Request) {
	var schemes []string

	for _, name := range pg.Config.PasswordSchemeOrdering {
		scheme, err := pg.Config.PasswordSchemes.Get(strings.ToLower(name))
		if err != nil {
			log.Printf("password scheme in sort list not configured: %s", name)
			http.Error(w, "internal server error", http.StatusInternalServerError)
			return
		}
		schemes = append(schemes, fmt.Sprintf("%s     (%d chars)", scheme.Description, scheme.Length))
	}

	yielddata := PassGenCreateForm{
		CsrfField:       csrf.Token(r),
		PasswordSchemes: schemes,
	}
	data := views.Data{Yield: yielddata}
	pg.IndexView.Render(w, data)
}

// WebAPICreate is called when a POST request from the the Index page is received to
// create a new password (Create from CRUD).
// It takes the sent form data, lookup for the requested password scheme
// and generated a password based on that scheme.
func (pg *PasswordGenerator) WebAPICreate(w http.ResponseWriter, r *http.Request) {
	// Try to parse the JSON POST body to an APIPasswordRequest structure.
	var requestData WebAPIPasswordRequest
	err := parseJSON(r, &requestData)
	if err != nil {
		log.Printf("invalid JSON data: %v", err)
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}

	// Convert the retrieved scheme ID from string to an integer.
	// The ID represents the position in the password PasswordSChemeOrdering list.
	// Based on this name the requested scheme will be looked up later.
	if requestData.SchemeID == "" {
		log.Printf("invalid JSON data (scheme ID): %v", "empty ID")
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}
	id, err := strconv.Atoi(requestData.SchemeID)
	if err != nil {
		log.Printf("invalid JSON data (scheme ID): %v", err)
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}
	if id < 0 {
		log.Printf("invalid JSON data (scheme ID): %s", "ID to small")
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}
	if id >= len(pg.Config.PasswordSchemeOrdering) {
		log.Printf("invalid JSON data (scheme ID): %s", "ID to large")
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}
	schemename := pg.Config.PasswordSchemeOrdering[id]
	//log.Printf("Password-Scheme: ID=%d, Name=%s", id, schemename)
	//log.Printf("Password-Schemes: %#v", pg.Config.PasswordSchemes)

	// try to find the specified password scheme
	scheme, err := pg.Config.PasswordSchemes.Get(strings.ToLower(schemename))
	if err != nil {
		log.Printf("invalid JSON data: %v (%v)", "password scheme not found", err)
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}

	// generate the password based on the specified pattern
	generator, err := password.NewGenerator(&password.GeneratorInput{
		Symbols: scheme.AllowedSymbols,
	})
	if err != nil {
		log.Printf("failed to initialize password generator: %v", err)
		http.Error(w, "password generation failed", http.StatusBadRequest)
		return
	}

	generatedPassword, err := generator.Generate(
		scheme.Length,
		scheme.NumDigits,
		scheme.NumSymbols,
		scheme.NoUpper,
		scheme.AllowRepeat,
	)
	if err != nil {
		log.Printf("failed to generate password: %v", err)
		http.Error(w, "password generation failed", http.StatusBadRequest)
		return
	}

	log.Printf("new password generated (scheme %s, ID %d)", scheme.Description, id)

	// generate a JSON response, used by the AJAX post request
	response := WebAPIPasswordResponse{Password: generatedPassword, UsedPattern: scheme.Description}
	if err := sendJSON(w, &response); err != nil {
		log.Printf("failed to send JSON result: %v", err)
		return
	}
}

// APICreate is called when a POST request from the the Index page is received to
// create a new password (Create from CRUD).
// It takes the sent form data, lookup for the requested password scheme
// and generated a password based on that scheme.
func (pg *PasswordGenerator) APICreate(w http.ResponseWriter, r *http.Request) {
	// Try to parse the JSON POST body to an APIPasswordRequest structure.
	var requestData APIPasswordRequest
	err := parseJSON(r, &requestData)
	if err != nil {
		log.Printf("invalid JSON data: %v", err)
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}

	// Validate the used API key, if the key doesn't match, we can abort.
	if (requestData.APIKey == "") || (requestData.APIKey != pg.Config.APIKey) {
		http.Error(w, "authentication failed", http.StatusForbidden)
		return
	}

	// Convert the retrieved scheme ID from string to an integer.
	// The ID represents the position in the password PasswordSChemeOrdering list.
	// Based on this name the requested scheme will be looked up later.
	if requestData.SchemeID == "" {
		log.Printf("empty scheme ID, using default (%s)", DEFAULT_SCHEMAID_FOR_API)
		requestData.SchemeID = DEFAULT_SCHEMAID_FOR_API
	}
	id, err := strconv.Atoi(requestData.SchemeID)
	if err != nil {
		log.Printf("invalid JSON data (scheme ID): %v", err)
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}
	if id < 0 {
		log.Printf("invalid JSON data (scheme ID): %s", "ID to small")
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}
	if id >= len(pg.Config.PasswordSchemeOrdering) {
		log.Printf("invalid JSON data (scheme ID): %s", "ID to large")
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}
	schemename := pg.Config.PasswordSchemeOrdering[id]
	//log.Printf("Password-Scheme: ID=%d, Name=%s", id, schemename)
	//log.Printf("Password-Schemes: %#v", pg.Config.PasswordSchemes)

	// try to find the specified password scheme
	scheme, err := pg.Config.PasswordSchemes.Get(strings.ToLower(schemename))
	if err != nil {
		log.Printf("invalid JSON data: %v (%v)", "password scheme not found", err)
		http.Error(w, "invalid JSON data", http.StatusBadRequest)
		return
	}

	// generate the password based on the specified pattern
	generator, err := password.NewGenerator(&password.GeneratorInput{
		Symbols: scheme.AllowedSymbols,
	})
	if err != nil {
		log.Printf("failed to initialize password generator: %v", err)
		http.Error(w, "password generation failed", http.StatusBadRequest)
		return
	}

	generatedPassword, err := generator.Generate(
		scheme.Length,
		scheme.NumDigits,
		scheme.NumSymbols,
		scheme.NoUpper,
		scheme.AllowRepeat,
	)
	if err != nil {
		log.Printf("failed to generate password: %v", err)
		http.Error(w, "password generation failed", http.StatusBadRequest)
		return
	}

	base64password := base64.StdEncoding.EncodeToString([]byte(generatedPassword))

	log.Printf("new password generated (scheme %s, ID %d)", scheme.Description, id)

	// generate a JSON response, used by the AJAX post request
	response := APIPasswordResponse{Password: generatedPassword, PasswordBase64: base64password, UsedPattern: scheme.Description}
	if err := sendJSON(w, &response); err != nil {
		log.Printf("failed to send JSON result: %v", err)
		return
	}
}
