package controllers

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

// A teststruct is used to marshal and unmarshal JSON data
// during the various test functions.
type teststruct struct {
	Magic    string `json:"magic"`
	Password string `json:"password"`
}

// TestParseJSON tests the parseJSON() function
func TestParseJSON(t *testing.T) {
	// out testcases
	testcases := []struct {
		name   string
		r      *http.Request
		result teststruct
	}{
		{
			"case1",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/secret/mymagic?password=mypass",
				strings.NewReader(`{"magic": "mymagic", "password": "mypass"}`)),
			teststruct{
				Magic:    "mymagic",
				Password: "mypass",
			},
		},
		{
			"case2",
			httptest.NewRequest(
				"POST",
				"http://localhost:8080/secret/mymagic?password=mypass",
				strings.NewReader(`{"password": "mypass"}`)),
			teststruct{
				Password: "mypass",
			},
		},
	}

	// loop over each test case and validate the result
	// in this case, the parseJSON fills a structure based on a JSON
	// http body.
	for _, tc := range testcases {
		var test teststruct
		if err := parseJSON(tc.r, &test); err != nil {
			t.Errorf("%s: failed: %v", tc.name, err)
		}
		if !reflect.DeepEqual(tc.result, test) {
			t.Errorf("%s: result is different: %v", tc.name, test)
		}
	}
}

// TestSendJSON tests the sendJSON() function
func TestSendJSON(t *testing.T) {
	// out testcases
	testcases := []struct {
		name              string
		data              teststruct
		statuscode        int
		contentTypeHeader string
		expected          string
	}{
		{
			"case1",
			teststruct{
				Magic:    "mymagic",
				Password: "mypass",
			},
			200,
			"application/json; charset=utf-8",
			`{"magic":"mymagic","password":"mypass"}`,
		},
		{
			"case2",
			teststruct{
				Magic:    "mymagic",
				Password: "",
			},
			200,
			"application/json; charset=utf-8",
			`{"magic":"mymagic","password":""}`,
		},
	}

	// loop over each test case and validate the result
	// in this case, the sendJSON takes a structure, convert it to JSON
	// and send everything to an http.ResponseWriter.
	// Result code, Content-type and body content have to match.
	for _, tc := range testcases {
		w := httptest.NewRecorder()
		sendJSON(w, &tc.data)
		resp := w.Result()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("failed to read body")
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statuscode {
			t.Errorf("%s: expected statuscode is different: %d", tc.name, resp.StatusCode)
		}

		if resp.Header.Get("Content-Type") != tc.contentTypeHeader {
			t.Errorf("%s: expected content-type header is different: %s", tc.name, resp.Header.Get("Content-Type"))
		}

		if string(body) != tc.expected {
			t.Errorf("%s: expected body is different: %s", tc.name, body)
		}
	}

}

//TODO: we trust the go stdlib: func TestParseForm(t *testing.T)
//TODO: we trust the go stdlib: func TestParseQuery(t *testing.T)
