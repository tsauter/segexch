package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/schema"
)

// parseJSON extracts the body text of an POST request
// and unmarshal them to the passed in structure.
// The data must be in JSON format.
func parseJSON(r *http.Request, dst interface{}) error {
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	err := decoder.Decode(dst)
	if err != nil {
		return err
	}

	return nil
}

// sendJSON takes a structure, marshel that to JSON and send
// the data back to an http.ResponseWriter.
// The correct HTTP status code and headers (Content-Type, etc. ) are set as well.
func sendJSON(w http.ResponseWriter, src interface{}) error {
	data, err := json.Marshal(src)
	if err != nil {
		return err
	}

	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s", data)
	return nil
}

// parseForm reads the PostForm field of an http.Request an
// unmarshal them to the passed in structure.
// This contains only the body.
func parseForm(r *http.Request, dst interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}
	dec := schema.NewDecoder()

	return dec.Decode(dst, r.PostForm)
}

// parseForm reads the Form field of an http.Request an
// unmarshal them to the passed in structure.
// This contains query parameters AND the body
func parseQuery(r *http.Request, dst interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}
	dec := schema.NewDecoder()
	return dec.Decode(dst, r.Form)
}
