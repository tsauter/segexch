package models

import (
	"testing"
	"time"

	"github.com/jinzhu/gorm"
	// this blank import is used to import the mysql support for Gorm
	"database/sql/driver"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/satori/go.uuid"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"

	"gitlab.com/tsauter/segexch/config"
)

var _ SecretService = &secretGorm{}

type AnyTime struct{}

func (a AnyTime) Match(v driver.Value) bool {
	_, ok := v.(time.Time)
	return ok
}

func newSecretMockGorm(config *config.Config) (*secretGorm, sqlmock.Sqlmock, error) {
	// create a database mock
	db, mock, err := sqlmock.New()
	if err != nil {
		return nil, nil, err
	}

	// initialize gorm with the mock
	gormDB, err := gorm.Open("mysql", db)
	if err != nil {
		return nil, nil, err
	}
	//gormDB.LogMode(true)

	sg := secretGorm{
		gormDB,
		0,
	}
	return &sg, mock, nil
}

// TestByMagic tests the ByMagic() function.
// TODO: this test is not working!!!
func TestByMagic(t *testing.T) {
	// check the Secret gorm with mocking
	sg, mock, err := newSecretMockGorm(nil)
	if err != nil {
		t.Fatalf("failed to initialize secret gorm mock: %v", err)
	}

	// the test cases
	testcases := []struct {
		name   string
		secret *Secret
		rows   *sqlmock.Rows
	}{
		{
			name: "case1",
			// create a full secret to make sure we only query the required/allow fields
			secret: &Secret{
				ID:                  0,
				CreatedAt:           time.Now().UTC(),
				UpdatedAt:           time.Now().UTC(),
				MagicUUID:           uuid.NewV4(),
				MagicEncrypted:      "myencryptedmagic",
				Password:            "mypassword",
				SecretText:          "mysecret",
				SecretTextEncrypted: "myencryptedsecret",
				ExpiryAt:            time.Now().UTC(),
			},
			//TODO: rows: sqlmock.NewRows([]string{"id", "title"}).AddRow(0, "xx"),
		},
		{
			name: "case2",
			// create a full secret to make sure we only query the required/allow fields
			secret: &Secret{
				ID:                  1,
				CreatedAt:           time.Now().UTC(),
				UpdatedAt:           time.Now().UTC(),
				MagicUUID:           uuid.NewV4(),
				MagicEncrypted:      "myencryptedmagic",
				Password:            "mypassword",
				SecretText:          "mysecret",
				SecretTextEncrypted: "myencryptedsecret",
				ExpiryAt:            time.Now().UTC(),
			},
			//TODO: rows: sqlmock.NewRows([]string{"id", "title"}).AddRow(0, "xx"),
		},
	}

	// loop over the test cases
	for _, tc := range testcases {
		// fill the mock with our expectations
		mock.ExpectQuery("SELECT").WithArgs( // TODO:statement
			tc.secret.MagicUUID.String(),
		).WillReturnRows(tc.rows)

		t.Logf("%v", sg)
		// call the function and validate the results
		// TODO: enable
		//secret := sg.ByMagic(*tc.secret)
		//if secret == nil {
		//t.Errorf("%s: ByMagic() function returned empty secret", tc.name)
		//}

		//if err := mock.ExpectationsWereMet(); err != nil {
		//t.Errorf("%s: mock: there were unfulfilled expectations: %v", tc.name, err)
		//}
	}
}

// TestCreate tests the Create() function.
func TestCreate(t *testing.T) {
	// check the Secret gorm with mocking
	sg, mock, err := newSecretMockGorm(nil)
	if err != nil {
		t.Fatalf("failed to initialize secret gorm mock: %v", err)
	}

	// the test cases
	testcases := []struct {
		name   string
		secret *Secret
	}{
		{
			name: "case1",
			// create a full secret to make sure we only store the required/allow fields
			secret: &Secret{
				ID:                  0,
				CreatedAt:           time.Now().UTC(),
				UpdatedAt:           time.Now().UTC(),
				MagicUUID:           uuid.NewV4(),
				MagicEncrypted:      "myencryptedmagic",
				Password:            "mypassword",
				SecretText:          "mysecret",
				SecretTextEncrypted: "myencryptedsecret",
				ExpiryAt:            time.Now().UTC(),
			},
		},
		{
			name: "case2",
			// create a full secret to make sure we only store the required/allow fields
			secret: &Secret{
				ID:                  1,
				CreatedAt:           time.Now().UTC(),
				UpdatedAt:           time.Now().UTC(),
				MagicUUID:           uuid.NewV4(),
				MagicEncrypted:      "myencryptedmagic",
				Password:            "mypassword",
				SecretText:          "mysecret",
				SecretTextEncrypted: "myencryptedsecret",
				ExpiryAt:            time.Now().UTC(),
			},
		},
	}

	// loop over the test cases
	for _, tc := range testcases {
		// fill the mock with our expectations
		mock.ExpectBegin()
		mock.ExpectExec("INSERT INTO `secrets` \\(`created_at`,`updated_at`,`magic_uuid`,`secret_text_encrypted`,`expiry_at`\\) VALUES \\(\\?,\\?,\\?,\\?,\\?\\)").WithArgs(
			AnyTime{},
			AnyTime{},
			tc.secret.MagicUUID.String(),
			"myencryptedsecret",
			tc.secret.ExpiryAt,
		).WillReturnResult(sqlmock.NewResult(1, 1))
		mock.ExpectCommit()

		// call the function and validate the results
		if err := sg.Create(tc.secret); err != nil {
			t.Errorf("%s: Create() function returned an error: %v", tc.name, err)
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("%s: mock: there were unfulfilled expectations: %v", tc.name, err)
		}
	}
}

/*
// nothing to check
func (sg *secretGorm) StartCleaner(ctx context.Context) error
*/

// TestCleaner tests the cleaner() function.
func TestCleaner(t *testing.T) {
	// check the Secret gorm with mocking
	sg, mock, err := newSecretMockGorm(nil)
	if err != nil {
		t.Fatalf("failed to initialize secret gorm mock: %v", err)
	}

	// fill the mock with our expectations
	mock.ExpectBegin()
	mock.ExpectExec("DELETE FROM `secrets` WHERE \\(expiry_at < \\?\\)").WithArgs(
		AnyTime{},
	).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	// call the function and validate the results
	numRows, err := sg.cleaner()
	if err != nil {
		t.Fatalf("cleaner() function returned an error: %v", err)
	}
	if numRows != 1 {
		t.Errorf("cleaner returned different number of elements: %d", numRows)
	}
}

// TestCleaner tests the cleaner() function.
func TestClose(t *testing.T) {
	// check the Secret gorm with mocking
	sg, mock, err := newSecretMockGorm(nil)
	if err != nil {
		t.Fatalf("failed to initialize secret gorm mock: %v", err)
	}

	// fill the mock with our expectations
	mock.ExpectClose()

	// call the function and validate the results
	err = sg.Close()
	if err != nil {
		t.Fatalf("Close() function returned an error: %v", err)
	}
}

/*
// nothing to check here
func (sg *secretGorm) DestructiveReset()
func (sg *secretGorm) AutoMigrate()
*/
