package modelmock

import (
	"context"

	"github.com/satori/go.uuid"

	"gitlab.com/tsauter/segexch/models"
)

var (
	testSecrets = map[string]models.Secret{
		"secret1": models.Secret{
			MagicUUID:           uuid.UUID{0x76, 0xa2, 0x78, 0x46, 0xbc, 0xa4, 0x43, 0x36, 0xb6, 0x77, 0x8c, 0x5f, 0xeb, 0xbb, 0x19, 0x43},
			MagicEncrypted:      "76a27846-bca4-4336-b677-8c5febbb1943",
			Password:            "12345",
			SecretText:          "mytext1",
			SecretTextEncrypted: "mytext1",
		},
		"secret2": models.Secret{
			MagicUUID:           uuid.UUID{0x20, 0x89, 0xcd, 0x7a, 0x3, 0xed, 0x43, 0x67, 0x83, 0x5b, 0x8e, 0x4d, 0x9f, 0x80, 0x5d, 0x70},
			MagicEncrypted:      "2089cd7a-03ed-4367-835b-8e4d9f805d70",
			Password:            "12345",
			SecretText:          "mytext2",
			SecretTextEncrypted: "mytext2",
		},
	}
)

// A SecretServiceMock can be used to emulate a real SecretService back.
// The mock omits the following:
// - no database is used
// - no real data
// - no encryption of any data
type SecretServiceMock struct {
}

// ByMagic returns the Secret with the given UUID. This mock use a simple
// slice, and the keys are not encrypted.
// The entry will not deleted from the slice.
func (ssm *SecretServiceMock) ByMagic(untrustedSecret models.Secret) *models.Secret {
	secret, ok := testSecrets[untrustedSecret.MagicEncrypted]
	if !ok {
		return nil
	}

	return &secret
}

// Create appends the given Secret to the slice. No additional checks or
// data conversation.
func (ssm *SecretServiceMock) Create(secret *models.Secret) error {
	*secret = testSecrets["secret1"]
	return nil
}

// AutoMigrate does nothing here. We do not need to alter any struct in this mock.
func (ssm *SecretServiceMock) AutoMigrate() {
	// nothing
}

// DestructiveReset does nothing here. We do not need to reset/delete any data in this mock.
func (ssm *SecretServiceMock) DestructiveReset() {
	// nothing
}

// StartCleaner does nothing here. We do not need to delete any expired data in this mock.
func (ssm *SecretServiceMock) StartCleaner(ctx context.Context) error {
	return nil
}
