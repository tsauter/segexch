package models

import (
	"fmt"
	"testing"
)

// TestError tests the Error() function
func TestError(t *testing.T) {
	expected := "The following fields are invalid: field1, field2"

	fes := FieldErrors{}
	fes["field1"] = fmt.Errorf("field1 is invalid")
	fes["field2"] = fmt.Errorf("field2 is to long")

	errstr := fes.Error()
	if errstr != expected {
		t.Errorf("string is different: %s", errstr)
	}
}
