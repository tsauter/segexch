package models

//
// Code from: https://www.calhoun.io/creating-random-strings-in-go/
//

import (
	"math/rand"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))

// StringWithCharset returns a random string with the specified length out of the
// available characters specified in charset.
func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// String returns a random string with the specified length.
func String(length int) string {
	return StringWithCharset(length, charset)
}
