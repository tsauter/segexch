package models

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

// make sure we always satisfy the interface
var _ SecretService = secretValidator{}

var (
	SampleSecrets = map[string]Secret{
		"case1": Secret{
			SecretText: " test string ",
			ExpiryAt:   time.Date(2017, 12, 06, 10, 30, 10, 0, time.UTC),
		},
		"case2": Secret{
			SecretText: "test string",
			ExpiryAt:   time.Date(2017, 06, 16, 22, 21, 10, 0, time.UTC),
		},
	}
)

// TestByMagic tests ByMagic()
/*
TODO: does this check make sense? high level?
func (sv secretValidator) ByMagic(untrustedSecret Secret) *Secret {
func TestByMagic(t *testing.T) {
}
*/

// TestCreate tests Create()
/*
TODO: does this check make sense? high level?
func (sv secretValidator) Create(secret *Secret) error {
func TestCreate(t *testing.T) {
}
*/

// TestOnCreate tests onCreate()
/*
FIXME: looks like the functions always get new memory addresses, so a simple evaluation is not possible
func TestOnCreate(t *testing.T) {
	// create the service
	sv := secretValidator{}

	expectedMap := map[string][]func(*Secret) error{
		// validate/alter field magic
		"magic": []func(*Secret) error{
			sv.generateMagic,
			requireEncryptedMagic,
			sv.magicIsAvailable,
			normalizeSecretText,
			sv.encryptSecretText,
		},
		// validate/alter field expiry
		"expiry": []func(*Secret) error{
			sv.setExpiryAt,
		},
	}

	returnedMap := sv.onCreate()

	getFun := func(i interface{}) string {
		return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
	}

	fmt.Printf("%v\n", getFun(returnedMap["magic"][0]))

	if !reflect.DeepEqual(returnedMap, expectedMap) {
		t.Errorf("returned secret is different: %v", returnedMap)
	}
}
*/

// TestRunSecretValidationFuncs tests runSecretValidationFuncs()
func TestRunSecretValidationFuncs(t *testing.T) {
	// create sample validator functions, this helps
	// to isolate the test case
	testValidater1 := func(secret *Secret) error {
		secret.MagicEncrypted = "testValidater1"
		return nil
	}
	testValidater2 := func(secret *Secret) error {
		secret.SecretTextEncrypted = "testValidater2"
		return nil
	}
	testValidaterError := func(secret *Secret) error {
		return fmt.Errorf("test error")
	}

	// our test cases
	testcases := map[string]struct {
		secret         Secret
		valFuncs       []func(*Secret) error
		expectedError  bool
		expectedSecret Secret
	}{
		"case1": {
			secret:         Secret{},
			valFuncs:       []func(*Secret) error{testValidater1, testValidater2},
			expectedError:  false,
			expectedSecret: Secret{MagicEncrypted: "testValidater1", SecretTextEncrypted: "testValidater2"},
		},
		// this test has en error in the middle and should therefor stop after the first function
		"case2": {
			secret:         Secret{},
			valFuncs:       []func(*Secret) error{testValidater1, testValidaterError, testValidater2},
			expectedError:  true,
			expectedSecret: Secret{MagicEncrypted: "testValidater1"},
		},
	}

	//func runSecretValidationFuncs(secret *Secret, fns ...func(*Secret) error) error {
	for tcname, compare := range testcases {
		// execute the function
		// errors are ignored, the validater functions may return errors
		err := runSecretValidationFuncs(&compare.secret, compare.valFuncs...)
		berr := (err == nil)
		if berr == compare.expectedError {
			t.Errorf("%s: returned an error is different than expected: %v", tcname, err)
		}

		if !reflect.DeepEqual(compare.secret, compare.expectedSecret) {
			t.Errorf("%s: returned secret is different: %v", tcname, compare.secret)
		}
	}
}

// TestRunSecretValidationMap tests runSecretValidationMap()
func TestRunSecretValidationMap(t *testing.T) {
	// create sample validator functions, this helps
	// to isolate the test case
	testValidater1 := func(secret *Secret) error {
		secret.MagicEncrypted = "testValidater1"
		return nil
	}
	testValidater2 := func(secret *Secret) error {
		secret.SecretTextEncrypted = "testValidater2"
		return nil
	}
	testValidaterError := func(secret *Secret) error {
		return fmt.Errorf("test error")
	}

	// our test cases
	testcases := map[string]struct {
		secret         Secret
		valFuncs       []func(*Secret) error
		expectedError  bool
		expectedSecret Secret
	}{
		"case1": {
			secret:         Secret{},
			valFuncs:       []func(*Secret) error{testValidater1, testValidater2},
			expectedError:  false,
			expectedSecret: Secret{MagicEncrypted: "testValidater1", SecretTextEncrypted: "testValidater2"},
		},
		// this test has en error in the middle and should therefor stop after the first function
		"case2": {
			secret:         Secret{},
			valFuncs:       []func(*Secret) error{testValidater1, testValidaterError, testValidater2},
			expectedError:  true,
			expectedSecret: Secret{MagicEncrypted: "testValidater1"},
		},
	}

	for tcname, compare := range testcases {
		// prepare a secret and initialize the map with our two check functions
		// the fieldname doesn't really matter here
		checks := make(map[string][]func(*Secret) error)
		checks[tcname] = compare.valFuncs

		// execute the function
		// errors are ignored, the validater functions may return errors
		err := runSecretValidationMap(&compare.secret, checks)
		berr := (err == nil)
		if berr == compare.expectedError {
			t.Errorf("%s: returned an error is different than expected: %v", tcname, err)
		}

		if !reflect.DeepEqual(compare.secret, compare.expectedSecret) {
			t.Errorf("%s: returned secret is different: %v", tcname, compare.secret)
		}
	}
}

// TestDecryptMagic tests decryptMagic()
func TestDecryptMagic(t *testing.T) {
	// our test cases
	// this is the way we get a secret to create a new text case
	//id := uuid.NewV4()
	//encMagicKeyBase64, _ := Encrypt(id.String(), compare.salt, compare.secret.Password)
	testcases := map[string]struct {
		salt              string
		secret            Secret
		expectedDecrypted string
	}{
		"case1": {
			salt: "test",
			secret: Secret{
				MagicEncrypted: "E5P2-uiRUn6blq_dW2DQNe1WfeaYF3TaJLVQEuyDn3jTjtsgQBn15mNxbfRxgkA2TQl6Q5n_lt8XdfeUkAEbop6X8-A",
				Password:       "12345678901234567890123456789012",
			},
			expectedDecrypted: "26c9eb3e-e8f2-44c3-b601-a84a40f8610f",
		},
		"case2": {
			salt: "1234567890",
			secret: Secret{
				MagicEncrypted: "Mi9E2OZHlpB1R2rBRG-DYmdxh1cIWKfSOusfmu5kofmk_OL6cT2gQLpTMbU2p9sxkZgTubheSG4p_98iCGmysfITdqgjg4GfedI",
				Password:       "09876543210987654321098765432109",
			},
			expectedDecrypted: "952595ca-4171-426c-9fee-66049a0d0e8e",
		},
		"case3": {
			salt: "fkjadsl837/&(/&)(/hfkjshfkjadsl837/&(/&)(/hfkjshk",
			secret: Secret{
				MagicEncrypted: "FTwn_Yir_jlkPiHsrruWyXlUzwEqe2o7kDhUplRwKf28G1N37mIlY3qLS8rzAdcRT0FYe5xDKboGS3SeaSS6Yf7pD1mNeuo_XkN6LLvAqTIR7dAgj_UHJif9zxobt1Pl3n6kpqoup2SHJ9rF4xvQT_4",
				Password:       "12121212121212121212121212121212",
			},
			expectedDecrypted: "06f96367-9666-49e5-968f-0b00a95d4df1",
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		// create the service and the secret
		sv := secretValidator{EncryptionSalt: compare.salt}

		// run the decrypt function
		err := sv.decryptMagic(&compare.secret)
		if err != nil {
			t.Fatalf("%s: function returned an error: %v", tcname, err)
		}

		// make sure the decrypted magic match our expectations
		if compare.secret.MagicUUID.String() != compare.expectedDecrypted {
			t.Errorf("%s: decrypted magic is different: %s", tcname, compare.secret.MagicUUID.String())
		}
	}
}

// TestDecryptText tests decryptText()
func TestDecryptText(t *testing.T) {
	// our test cases
	// this is the way we get a secret to create a new text case
	//encMagicKeyBase64, _ := Encrypt(compare.expectedDecrypted, compare.salt, compare.secret.Password)
	//fmt.Printf("%s: %s\n", tcname, encMagicKeyBase64)
	testcases := map[string]struct {
		salt              string
		secret            Secret
		expectedDecrypted string
	}{
		"case1": {
			salt: "test",
			secret: Secret{
				SecretTextEncrypted: "UGxyM1FO1ZIentiABYgHnPsMIm6oU2CwgkfGW7YSV4V3BdcXUKFeistNPPZfe9i5ITg-9Q",
				Password:            "12345678901234567890123456789012",
			},
			expectedDecrypted: "my super secret text",
		},
		"case2": {
			salt: "1234567890",
			secret: Secret{
				SecretTextEncrypted: "SJTYBRQkNIbj-Q16hfM3YBf5C5IONlzQRiCZJWrmd-_cSjOV2oJrpUGbbjtsaoCrPmQqRLI76A",
				Password:            "09876543210987654321098765432109",
			},
			expectedDecrypted: "short secret text",
		},
		"case3": {
			salt: "fkjadsl837/&(/&)(/hfkjshfkjadsl837/&(/&)(/hfkjshk",
			secret: Secret{
				SecretTextEncrypted: "8yTxvd-A_WzrXA-NmyqOomc7ZA6mGb5jPh1FMGs9kuD8Xkl8FCGYekliHK9CEMH3qxpgq2hRLW6cb08UkOWgSXbMpVP4DSVOIQlLOCp63ATQ0UDyqfnTx8naAC4",
				Password:            "12121212121212121212121212121212",
			},
			expectedDecrypted: "<html>xx</html>",
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		// create the service and the secret
		sv := secretValidator{EncryptionSalt: compare.salt}

		// run the decrypt function
		err := sv.decryptText(&compare.secret)
		if err != nil {
			t.Fatalf("%s: function returned an error: %v", tcname, err)
		}

		// make sure the decrypted text match our expectations
		if compare.secret.SecretText != compare.expectedDecrypted {
			t.Errorf("%s: decrypted text is different: %s", tcname, compare.secret.SecretText)
		}
	}
}

// TestGenerateMagic tests generateMagic()
func TestGenerateMagic(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		salt string
	}{
		"case1": {
			salt: "test",
		},
		"case2": {
			salt: "1234567890",
		},
		"case3": {
			salt: "fkjadsl837/&(/&)(/hfkjshfkjadsl837/&(/&)(/hfkjshk",
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		// create the service and the secret
		sv := secretValidator{EncryptionSalt: compare.salt}
		secret := Secret{}

		// generate the magic and the password
		err := sv.generateMagic(&secret)
		if err != nil {
			t.Errorf("%s: function returned an error: %v", tcname, err)
		}

		// make sure all requested fields have values
		if secret.MagicUUID.String() == "" {
			t.Errorf("%s: MagicUUID is empty", tcname)
		}
		if secret.Password == "" {
			t.Errorf("%s: Password is empty", tcname)
		}
		if secret.MagicEncrypted == "" {
			t.Errorf("%s: MagicEncrypted is empty", tcname)
		}

		// try decrypting again
		_, err = Decrypt(secret.MagicEncrypted, sv.EncryptionSalt, secret.Password)
		if err != nil {
			t.Errorf("%s: decryption function returned an error: %v", tcname, err)
		}
	}
}

// TODO: create a test?
//func (sv secretValidator) magicIsAvailable(secret *Secret) error {

// TestEncryptSecretText tests encryptSecretText()
func TestEncryptSecretText(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		salt       string
		password32 string
		text       string
	}{
		"case1": {
			salt:       "test",
			password32: "12345678901234567890123456789012",
			text:       "my message",
		},
		"case2": {
			salt:       "1234567890",
			password32: "12345678901234567890123456789012",
			text:       "my secret message",
		},
		"case3": {
			salt:       "fkjadsl837/&(/&)(/hfkjshfkjadsl837/&(/&)(/hfkjshk",
			password32: "12345kdja(/)(/094924920f/&%&9012",
			text:       "my secret message II",
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		// create the service and the secret
		sv := secretValidator{EncryptionSalt: compare.salt}
		secret := Secret{SecretText: compare.text, Password: compare.password32}

		// encrypt
		err := sv.encryptSecretText(&secret)
		if err != nil {
			t.Errorf("%s: encryption function returned an error: %v", tcname, err)
		}

		// make sure the original text was wipped
		if secret.SecretText != "" {
			t.Errorf("%s: SecretText is not empty", tcname)
		}

		// try decrypting again
		text, err := Decrypt(secret.SecretTextEncrypted, sv.EncryptionSalt, secret.Password)
		if err != nil {
			t.Errorf("%s: decryption function returned an error: %v", tcname, err)
		}

		// compare the original text and the decrypted text
		if text != compare.text {
			t.Errorf("%s: decrypted text is different: %s", tcname, text)
		}
	}
}

// TestSetExpiryAt tests setExpiryAt()
func TestSetExpiryAt(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		secret          Secret
		durationSeconds int
	}{
		"case1": {
			secret:          Secret{},
			durationSeconds: 60 * 60 * 24 * 1,
		},
		"case2": {
			secret:          Secret{},
			durationSeconds: 60 * 60,
		},
		"case3": {
			secret:          Secret{},
			durationSeconds: 60 * 60 * 24 * 31,
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		// create different expiry dates and validate the function
		// we add a window of 10 seconds +/- to to avoid runtime errors
		sv := secretValidator{MaxSecretLifetime: compare.durationSeconds}
		minDiff := time.Now().UTC().Add(time.Second * time.Duration(sv.MaxSecretLifetime)).Truncate(time.Second * 5)
		maxDiff := time.Now().UTC().Add(time.Second * time.Duration(sv.MaxSecretLifetime)).Add(time.Second * 5)

		err := sv.setExpiryAt(&compare.secret)
		if err != nil {
			t.Errorf("%s: function returned an error: %v", tcname, err)
		}

		if compare.secret.ExpiryAt.Before(minDiff) {
			t.Errorf("%s: expiry date is older than expected: %s", tcname, compare.secret.ExpiryAt)
		}
		if compare.secret.ExpiryAt.After(maxDiff) {
			t.Errorf("%s: expiry date is younger than expected: %s", tcname, compare.secret.ExpiryAt)
		}
	}
}

// TestRequireEncryptedMagic tests requireEncryptedMagic()
func TestRequireEncryptedMagic(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		secret          Secret
		shouldBeAllowed bool
	}{
		"case1": {
			secret:          Secret{MagicEncrypted: "testpasswort"},
			shouldBeAllowed: true,
		},
		"case2": {
			secret:          Secret{MagicEncrypted: "test passwort!!!% "},
			shouldBeAllowed: true,
		},
		"case3": {
			secret:          Secret{MagicEncrypted: ""},
			shouldBeAllowed: false,
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		err := requireEncryptedMagic(&compare.secret)
		berr := (err == nil)
		if berr != compare.shouldBeAllowed {
			t.Errorf("%s: returned an error is different than expected: %v", tcname, err)
		}
	}
}

// TestValidateEncryptedMagic tests the validateEncryptedMagic()
func TestValidateEncryptedMagic(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		secret          Secret
		shouldBeAllowed bool
	}{
		"case1": {
			secret:          Secret{MagicEncrypted: "testpasswort"},
			shouldBeAllowed: true,
		},
		"case2": {
			secret:          Secret{MagicEncrypted: "test passwort!!!% "},
			shouldBeAllowed: false,
		},
		"case3": {
			secret:          Secret{MagicEncrypted: "LpHuHWf_efGcDL7fX-byV-MXJ5k-o7SIc4xiDTZhNFbBF1xdE_wOXpWQbQT_Ni3XYwVb_8GOLmhpEk3t-emXxXcsrWwdSl5M98TEg3IdgAjXHUjlzBWBR3VqWgBcdsho"},
			shouldBeAllowed: true,
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		err := validateEncryptedMagic(&compare.secret)
		berr := (err == nil)
		if berr != compare.shouldBeAllowed {
			t.Errorf("%s: returned an error is different than expected: %v", tcname, err)
		}
	}
}

// TestRequirePassword tests the requirePassword()
func TestRequirePassword(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		secret          Secret
		shouldBeAllowed bool
	}{
		"case1": {
			secret:          Secret{Password: "testpasswort"},
			shouldBeAllowed: true,
		},
		"case2": {
			secret:          Secret{Password: "test passwort!!!% "},
			shouldBeAllowed: true,
		},
		"case3": {
			secret:          Secret{Password: ""},
			shouldBeAllowed: false,
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		err := requirePassword(&compare.secret)
		berr := (err == nil)
		if berr != compare.shouldBeAllowed {
			t.Errorf("%s: returned an error is different than expected: %v", tcname, err)
		}
	}
}

// TestValidatePassword tests the validatePassword()
func TestValidatePassword(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		secret          Secret
		shouldBeAllowed bool
	}{
		"case1": {
			secret:          Secret{Password: "testpasswort"},
			shouldBeAllowed: true,
		},
		"case2": {
			secret:          Secret{Password: "test passwort!!!% "},
			shouldBeAllowed: false,
		},
		"case3": {
			secret:          Secret{Password: "NFg1VUVveUYybTFCc3hoVkQ3M1hZWWpwdThDYVpkNzk"},
			shouldBeAllowed: true,
		},
	}

	// process each test case
	for tcname, compare := range testcases {
		err := validatePassword(&compare.secret)
		berr := (err == nil)
		if berr != compare.shouldBeAllowed {
			t.Errorf("%s: returned an error is different than expected: %v", tcname, err)
		}
	}
}

// TestNormalizeSecretText tests the normalizeSecretText()
func TestNormalizePassword(t *testing.T) {
	// how we cet the modified string?
	//pw := base64.RawURLEncoding.EncodeToString([]byte(compare.orig.Password))
	//fmt.Printf("%s\n", string(pw))

	// our test cases
	testcases := map[string]struct {
		orig     Secret
		modified Secret
	}{
		"case1": {
			orig:     Secret{Password: "test passwort!!!% "},
			modified: Secret{Password: "dGVzdCBwYXNzd29ydCEhISUg"},
		},
	}

	// process each test case
	// the SecretText should be correctly modified
	for tcname, compare := range testcases {

		err := normalizePassword(&compare.modified)
		if err != nil {
			t.Errorf("%s: returned an error: %v", tcname, err)
		}

		if !reflect.DeepEqual(compare.orig, compare.modified) {
			t.Errorf("%s: Secrets are different: %#v", tcname, compare.modified)
		}
	}
}

// TestValidateLifetime tests the validateLifetime()
func TestValidateLifetime(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		fakeExpiryDate time.Time
		errorExpected  bool
	}{
		"case1": {
			// not yet expired, function should return nil (ok)
			fakeExpiryDate: time.Now().UTC().Add(time.Hour * 1),
			errorExpected:  false,
		},
		"case2": {
			// already expired, function should return an error
			fakeExpiryDate: time.Now().UTC().Truncate(time.Hour * 1),
			errorExpected:  true,
		},
	}

	// process each test case
	// the "fakeExpiryDate" is used to set a fake expiry time
	// around the current the current time. Is is required because
	// of the anathomy validateLifetime() function. It uses time.Now().UTC()
	// internaly.
	for tcname, tcsample := range SampleSecrets {
		compare := testcases[tcname]
		tcsample.ExpiryAt = compare.fakeExpiryDate

		err := validateLifetime(&tcsample)
		berr := (err != nil)
		if berr != compare.errorExpected {
			t.Errorf("%s: returned an error is different than expected: %v", tcname, err)
		}
	}
}

// TestNormalizeSecretText tests the normalizeSecretText()
func TestNormalizeSecretText(t *testing.T) {
	// our test cases
	testcases := map[string]struct {
		expected Secret
	}{
		"case1": {
			expected: Secret{
				SecretText: "test string",
				ExpiryAt:   time.Date(2017, 12, 06, 10, 30, 10, 0, time.UTC),
			},
		},
		"case2": {
			expected: Secret{
				SecretText: "test string",
				ExpiryAt:   time.Date(2017, 06, 16, 22, 21, 10, 0, time.UTC),
			},
		},
	}

	// process each test case
	// the SecretText should be correctly modified
	for tcname, tcsample := range SampleSecrets {
		compare := testcases[tcname]

		err := normalizeSecretText(&tcsample)
		if err != nil {
			t.Errorf("%s: returned an error: %v", tcname, err)
		}

		if !reflect.DeepEqual(tcsample, compare.expected) {
			t.Errorf("%s: Secrets are different: %#v", tcname, tcsample)
		}
	}
}
