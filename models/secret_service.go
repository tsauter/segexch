package models

import (
	"gitlab.com/tsauter/segexch/config"
)

// NewSecretService returns a SecretService object.
func NewSecretService(config *config.Config) (SecretService, error) {
	sjf, err := newSecretGorm(config)
	if err != nil {
		return nil, err
	}

	srv := &secretValidator{
		SecretService:     sjf,
		EncryptionSalt:    config.EncryptionSalt,
		MaxSecretLifetime: config.MaxSecretLifetime,
	}
	return srv, nil
}
