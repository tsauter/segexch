package models

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/jinzhu/gorm"
	// this blank import is used to import the mysql support for Gorm
	_ "github.com/jinzhu/gorm/dialects/mysql"

	"gitlab.com/tsauter/segexch/config"
)

// A secretGorm represents a SecretService backend to load and store
// secrets to a real database.
// Gorm is used for a generic database wrapper around various poossible databases.
type secretGorm struct {
	// the Gorm interface
	*gorm.DB
	// CleanupInterval configures the interval of each cleanup cycle
	CleanupInterval int
}

// newSecretGorm initialize the database connection and configures
// gorm.
// All required informations are taken from the configuration object.
func newSecretGorm(config *config.Config) (*secretGorm, error) {
	// create a DB connection string; this string must match the database type
	connectionInfo := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		config.DBUser, config.DBPasswd, config.DBHost, config.DBPort, config.DBName)

	// initialize gorm, bases on the db type, with the created connectionInfo string
	db, err := gorm.Open(config.DBType, connectionInfo)
	if err != nil {
		return nil, err
	}
	db.LogMode(config.DBVerbose)

	// return the new created secretGorm struct
	return &secretGorm{db, config.CleanerInterval}, nil
}

// ByMagic queries the database for a Secret with the specified MagicUUID,
// the found secret will the immediatly deleted from the database.
// Additionaly, we span a safety net (the cleaner) to delete these secrets.
// The MagicUUID must be already decrypted and converted to a valid uuid.
func (sg *secretGorm) ByMagic(searchSecret Secret) *Secret {
	// query the database with gorm
	secret := sg.byQuery(sg.DB.Where("magic_uuid = ?", searchSecret.MagicUUID))

	// if a secret was found, remove this secret immediatly form the database
	if secret != nil {
		err := sg.DB.Delete(&secret).Error
		if err != nil {
			return nil
		}
	}

	return secret
}

// byQuery is a help function to abstract sql queries. Only the first found element
// will returned. If nothing was found an nil Secret instead of an database
// error is return.
func (sg *secretGorm) byQuery(query *gorm.DB) *Secret {
	ret := &Secret{}
	// return only the first element
	err := query.First(ret).Error
	switch err {
	case nil:
		return ret
	case gorm.ErrRecordNotFound:
		return nil
	default:
		panic(err)
	}
}

// Create stores the passed in Secret to the database.
// We always would like to have a new unique ID, this is why
// the ID will be reset everytime.
func (sg *secretGorm) Create(secret *Secret) error {
	secret.ID = 0 // do not allow own IDs
	return sg.DB.Create(secret).Error
}

// StartCleaner starts a new go function to start the database
// cleaner every XX seconds. After the start a clean will be immediatly
// fired. With that, we don't have to wait for the next ticker interval.
func (sg *secretGorm) StartCleaner(ctx context.Context) error {
	c := time.Tick(time.Second * time.Duration(sg.CleanupInterval))

	// start the first cleaner interval; otherwise we have to wait
	// for the first ticker event
	log.Printf("Starting initial cleaner...")
	numRows, err := sg.cleaner()
	if err != nil {
		log.Printf("Failed to purge expired secrets from database: %s", err)
		return err
	}

	log.Printf("Purged %d expired secrets from database", numRows)

	// initialize a Go function which wait forever and starts the cleaner
	// every tick interval
	go func() {
		for {
			select {
			case <-c:
				log.Printf("Starting cleaner...")
				numRows, err := sg.cleaner()
				if err != nil {
					log.Printf("Failed to purge expired secrets from database: %s", err)
				} else {
					log.Printf("Purged %d expired secrets from database", numRows)
				}
			}
		}
	}()

	return nil
}

// cleaner is the safety net, it will removed all expired secrets from the database
func (sg *secretGorm) cleaner() (int64, error) {
	result := sg.DB.Delete(Secret{}, "expiry_at < ?", time.Now().UTC())
	return result.RowsAffected, result.Error
}

// Close closes the connection to the database.
func (sg *secretGorm) Close() error {
	return sg.DB.Close()
}

// DestructiveReset resets the complete database!!!
// It drops all tables and recreates them.
// Gorm handle this for us.
func (sg *secretGorm) DestructiveReset() {
	sg.DropTableIfExists(&Secret{})
	sg.AutoMigrate()
}

// AutoMigrate creates and alters changed tables (based on models)
// Gorm handle this for us.
func (sg *secretGorm) AutoMigrate() {
	sg.DB.AutoMigrate(&Secret{})
}
