package models

import (
	"time"

	"github.com/satori/go.uuid"
)

// Secret represents a secret in the web site.
// Do not use gorm.Model here -> gorm softdelete
type Secret struct {
	ID                  uint `gorm:"primary_key"`
	CreatedAt           time.Time
	UpdatedAt           time.Time
	MagicUUID           uuid.UUID `gorm:"magic_uuid;not null;unique_index"`
	MagicEncrypted      string    `gorm:"-"`
	Password            string    `gorm:"-"`
	SecretText          string    `gorm:"-"`
	SecretTextEncrypted string    `gorm:"secret_text_encrypted"`
	ExpiryAt            time.Time `gorm:"expiry_at"`
}
