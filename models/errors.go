package models

import (
	"fmt"
	"sort"
	"strings"
)

// FieldErrors is used to return a map of errors for
// each form field. The name of the field is used as key name.
type FieldErrors map[string]error

// Error created a list of all fields and return them as a string.
func (fe FieldErrors) Error() string {
	keys := make([]string, 0, len(fe))
	for key := range fe {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return fmt.Sprintf("The following fields are invalid: %s", strings.Join(keys, ", "))
}
