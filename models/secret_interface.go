package models

import (
	"context"
)

// SecretService defines all of the methods we need to
// interact with the Secret resource.
type SecretService interface {
	ByMagic(untrustedSecret Secret) *Secret
	Create(secret *Secret) error
	SecretMigrator
	SecretCleaner
}

// SecretMigrator defines methods used to migrate and reset
// the secret database.
type SecretMigrator interface {
	AutoMigrate()
	DestructiveReset()
}

// SecretCleaner defines methods to keep the secret database
// clean and secure.
type SecretCleaner interface {
	StartCleaner(ctx context.Context) error
}
