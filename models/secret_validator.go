package models

import (
	"encoding/base64"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/satori/go.uuid"
)

// encryptedMagicRegex is a regular expression to validate incoming magic strings
var encryptedMagicRegex = regexp.MustCompile(`^[A-Za-z0-9_\-]+$`)

// passwordRegex is a regular expression to validate incoming password strings
var passwordRegex = regexp.MustCompile(`^[A-Za-z0-9]+$`)

// passwordLength specified the required length of the password. The password has to be exactly this lenght
var passwordLength = 32

// A secretValidator represents a SecretService frontend the validates
// converts and sanitize all date.
// It do all the jobs, that shouldn't be done by the database.
type secretValidator struct {
	SecretService
	EncryptionSalt    string
	MaxSecretLifetime int
}

// ByMagic takes a Secret do a lot of validation, query the database for the
// data and then validate again.
// This function also validates passwords and decrypt all encrypted properties.
func (sv secretValidator) ByMagic(untrustedSecret Secret) *Secret {
	// run prevalidation functions before passing the data to the
	// database backend
	if err := runSecretValidationFuncs(&untrustedSecret,
		requireEncryptedMagic,
		validateEncryptedMagic,
		requirePassword,
		validatePassword,
		normalizePassword,
		sv.decryptMagic,
	); err != nil {
		return nil
	}

	// query the backend
	secret := sv.SecretService.ByMagic(untrustedSecret)
	if secret == nil {
		return nil
	}

	// the backend knows nothing about the password, but
	// the password is required to decrypt the text later
	secret.Password = untrustedSecret.Password

	// run postvalidation functions after got the secret from database
	// this is necessary because of the backend which is completly blind
	// regarding data handling
	if err := runSecretValidationFuncs(secret,
		validateLifetime,
		requirePassword,
		validatePassword,
		sv.decryptText,
	); err != nil {
		return nil
	}

	return secret
}

// Create takes a partial secret, validate the necessary fields
// and then, if ok, passes the secret to the database backend to
// store the secret.
func (sv secretValidator) Create(secret *Secret) error {
	err := runSecretValidationMap(secret, sv.onCreate())
	if err != nil {
		return err
	}
	return sv.SecretService.Create(secret)
}

// onCreate is a helper function to list all function which have to be
// executed *before* the secret can be passed to the backend database.
// The name parts of the map should be the names of the form fields
// of the html page that renders the form for this function.
func (sv secretValidator) onCreate() map[string][]func(*Secret) error {
	return map[string][]func(*Secret) error{
		// validate/alter field magic
		"magic": []func(*Secret) error{
			sv.generateMagic,
			requireEncryptedMagic,
			sv.magicIsAvailable,
			normalizeSecretText,
			sv.encryptSecretText,
		},
		// validate/alter field expiry
		"expiry": []func(*Secret) error{
			sv.setExpiryAt,
		},
	}
}

// runSecretValidationFuncs loops of the passed in functions and execute
// each of it with the Secret. Each function can alter/modify parts of or
// the complete secret.
// Each function can return an error, which aborts the complete stack and
// returns the error for this wrapper function.
// This function is used to evaluate and modify user input.
func runSecretValidationFuncs(secret *Secret, fns ...func(*Secret) error) error {
	for _, fn := range fns {
		if err := fn(secret); err != nil {
			//log.Printf("%s", err)
			return err
		}
	}
	return nil
}

// runSecretValidationMap runs checks form fields. The name of the field and the
// associated functions are passed on through the map.
// Each returned error as added to an error collection. This collection can then
// later be used, to display the errors to the end user in the html page.
// If any error was returned, the complete function returns with an error.
// This function is used to evaluate user input and present them to the user.
func runSecretValidationMap(secret *Secret, m map[string][]func(*Secret) error) error {
	fieldErrors := make(FieldErrors, 0)
	for field, fns := range m {
		err := runSecretValidationFuncs(secret, fns...)
		if err != nil {
			fieldErrors[field] = err
		}
	}
	if len(fieldErrors) > 0 {
		// We had at least one error occur
		return fieldErrors
	}

	// No errors occured, so return nil. DO NOT return the
	// fieldErrors object, as it isn't nil and an
	// if err == nil check will fail.
	return nil
}

//
// *** VALIDATORS/MODIFIERS ******************************************************
//

// decryptMagic decrypts the specified magic string and create the original
// unique magic id. This id can be used to query the backend databases.
// For the decryption we need the encrypted magic, the SALT from config and the password.
// The encrypted magic has to be URL encoded; the salt/password have to be unencoded.
// The password is *not* store in the database, only the end user has it.
func (sv secretValidator) decryptMagic(secret *Secret) error {
	encMagicKeyBase64, err := Decrypt(secret.MagicEncrypted, sv.EncryptionSalt, secret.Password)
	if err != nil {
		return fmt.Errorf("magic decryption failed")
	}

	// convert the decrypted magic back to an UUID
	uuid, err := uuid.FromString(encMagicKeyBase64)
	if err != nil {
		return err
	}

	secret.MagicUUID = uuid

	return nil
}

// decryptText decrypts the stored secret text.
// For the decryption we need the encrypted text, the SALT from config and the password.
// The encrypted text has to be URL encoded; the salt/password have to be unencoded.
// The password is *not* store in the database, only the end user has it.
func (sv secretValidator) decryptText(secret *Secret) error {
	secretText, err := Decrypt(secret.SecretTextEncrypted, sv.EncryptionSalt, secret.Password)
	if err != nil {
		return fmt.Errorf("text decryption failed")
	}
	secret.SecretText = secretText

	return nil
}

// generateMagic generates an unique ID (we use the UUID package here).
// The ID will be encrypted and store in the secret.
// For the encryption we need text the ID, the SALT from config and the password.
// The salt has to be unencoded. The password is returned *unencoded*!
// The password is *not* store in the database, only the end user has it.
func (sv secretValidator) generateMagic(secret *Secret) error {
	// generate a unique UUID. Currently, we do not
	// add hostname, timestamp or instance data to the UUID.
	secret.MagicUUID = uuid.NewV4()
	secret.Password = String(passwordLength)

	encMagicKeyBase64, err := Encrypt(secret.MagicUUID.String(), sv.EncryptionSalt, secret.Password)
	if err != nil {
		return err
	}
	secret.MagicEncrypted = encMagicKeyBase64

	return nil
}

// magicIsAvailable query the database to be sure that the magic id is not yet in use.
// This may lead to a race condition, but this isn't a big deal. The database insert function
// later will cover this for us.
func (sv secretValidator) magicIsAvailable(secret *Secret) error {
	// First check to see if the magic is taken
	existing := sv.SecretService.ByMagic(*secret)
	if existing == nil {
		return nil
	}

	return nil
}

// encryptSecretText encryptes the text with the salt and password.
// For the encryption we need text, the SALT from config and the password.
// The salt/password have to be unencoded.
// The password is *not* store in the database, only the end user has it.
func (sv secretValidator) encryptSecretText(secret *Secret) error {
	text, err := Encrypt(secret.SecretText, sv.EncryptionSalt, secret.Password)
	if err != nil {
		return fmt.Errorf("failed to encrypt text: %v", err)
	}
	secret.SecretTextEncrypted = text
	// make sure the unencrypted text is immediatly blank after the encryption
	secret.SecretText = ""

	return nil
}

// setExpiryAt assigns a fixed expiry date to the secret. The date es calculated
// from the config setting MaxSecretLifetime.
func (sv secretValidator) setExpiryAt(secret *Secret) error {
	secret.ExpiryAt = time.Now().UTC().Add(time.Second * time.Duration(sv.MaxSecretLifetime))
	return nil
}

//
// *** simple VALIDATORS *********************************************************
//

// requireEncryptedMagic makes sure, that the MagicEncrypted value is not empty.
func requireEncryptedMagic(secret *Secret) error {
	if secret.MagicEncrypted == "" {
		return fmt.Errorf("encrypted magic missing")
	}
	return nil
}

// validateEncryptedMagic makes sure, that the MagicEncrypted value
// containts only allowed characters
func validateEncryptedMagic(secret *Secret) error {
	if !encryptedMagicRegex.MatchString(secret.MagicEncrypted) {
		return fmt.Errorf("magic string is invalid")
	}
	return nil
}

// requirePassword make sure that we have a non empty password
func requirePassword(secret *Secret) error {
	if secret.Password == "" {
		return fmt.Errorf("password for encryption missing")
	}
	return nil
}

// validatePassword makes sure, that the Password value
// containts only allowed characters
func validatePassword(secret *Secret) error {
	if !passwordRegex.MatchString(secret.Password) {
		return fmt.Errorf("password string is invalid")
	}
	return nil
}

// normalizePassword converts the password from URL encode to the real password
func normalizePassword(secret *Secret) error {
	pw, err := base64.RawURLEncoding.DecodeString(secret.Password)
	if err != nil {
		return fmt.Errorf("password string is invalid (base64)")
	}

	secret.Password = string(pw)

	return nil
}

// validateLifetime satisfies that the expiry date (lifetime) of
// the secret is not over. The date has to be before the current time (UTC).
func validateLifetime(secret *Secret) error {
	if secret.ExpiryAt.Before(time.Now().UTC()) {
		secret = nil
		return fmt.Errorf("Secret expired")
	}
	return nil
}

// normalizeSecretText cleans the SecretText a bit.
func normalizeSecretText(secret *Secret) error {
	secret.SecretText = strings.TrimSpace(secret.SecretText)
	return nil
}
