package models

import (
	"regexp"
	"testing"
)

// TestStringWithCharset tests the  StringWithCharset() function.
func TestStringWithCharset(t *testing.T) {
	testcases := []struct {
		name    string
		length  int
		charset string
		re      *regexp.Regexp
	}{
		{
			"case1",
			32,
			"abcde",
			regexp.MustCompile("[a-e]+"),
		},
		{
			"case2",
			16,
			"abcdefghikjklmnopqrstuvwxyz",
			regexp.MustCompile("[a-z]+"),
		},
	}

	for _, tc := range testcases {
		s := StringWithCharset(tc.length, tc.charset)

		if len(s) != tc.length {
			t.Errorf("%s: string has a different length: %d", tc.name, len(s))
		}
		if !tc.re.MatchString(s) {
			t.Errorf("%s: string does not match regex: %s", tc.name, s)
		}
	}

}

// TestString tests the String() function.
func TestString(t *testing.T) {
	testcases := []struct {
		name   string
		length int
		re     *regexp.Regexp
	}{
		{
			"case1",
			32,
			regexp.MustCompile("[A-Za-z]+"),
		},
	}

	for _, tc := range testcases {
		s := String(tc.length)

		if len(s) != tc.length {
			t.Errorf("%s: string has a different length: %d", tc.name, len(s))
		}
		if !tc.re.MatchString(s) {
			t.Errorf("%s: string does not match regex: %s", tc.name, s)
		}
	}

}
