package models

import (
	"encoding/base64"
	"fmt"
	"strings"

	"github.com/gtank/cryptopasta"
)

// Encrypt encryptes a plain text string. The encrypted string will be returned as an
// url encoded representation.
// The plain text string is appended by a salt the 32 char key is used for encryption.
func Encrypt(plainMagic string, magicSalt string, KV string) (string, error) {
	// KV have to be 32 characters long
	if len(KV) != 32 {
		return "", fmt.Errorf("key has to be 32 bit characters long")
	}

	// convert the KV string to a fix 32-byte slice
	var key [32]byte
	copy(key[:], []byte(KV))

	// prepend the salt and encrypt the text
	encMagicKey, err := cryptopasta.Encrypt([]byte(magicSalt+plainMagic), &key)
	if err != nil {
		return "", err
	}

	// return the encrypted text as an url encoded string
	// RawURLEncoding will not pad the string with "==" which are not allowed characters in URLs
	return base64.RawURLEncoding.EncodeToString([]byte(encMagicKey)), nil
}

// Decrypt decryptes an url encoded, encrypted string.
// The salt will be removed from the decrypted string. KV is the 32 bit key.
func Decrypt(cipherstring string, magicSalt string, KV string) (string, error) {
	// KV have to be 32 characters long
	if len(KV) != 32 {
		return "", fmt.Errorf("key has to be 32 bit characters long")
	}

	// convert the KV string to a fix 32-byte slice
	var key [32]byte
	copy(key[:], []byte(KV))

	// remove the url encoding
	saltedCipher, err := base64.RawURLEncoding.DecodeString(cipherstring)
	if err != nil {
		return "", err
	}

	// decrypt the string
	plaintext, err := cryptopasta.Decrypt(saltedCipher, &key)
	if err != nil {
		return "", err
	}

	// make sure the decrypted string starts with our salt
	if len(string(plaintext)) < (len(magicSalt) + 1) {
		return "", fmt.Errorf("decrypted string is to short")
	}
	if !strings.HasPrefix(string(plaintext), magicSalt) {
		return "", fmt.Errorf("invalid salt")
	}

	// remove the prefixed salt and convert to string
	// lenght of the string is already validates above
	stripped := string(plaintext)[len(magicSalt):]

	return stripped, nil
}
