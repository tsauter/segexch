package models

import (
	"testing"
)

// TestEncryptDecrypt tests both functions Encrypt() and Decrypt() together.
// This makes sure, that both function works together and understand each other.
func TestEncryptDecrypt(t *testing.T) {
	// test cases
	testcases := []struct {
		name      string
		plaintext string
		salt      string
		KV        string
	}{
		{
			name:      "case1",
			plaintext: "The quick brown fox jumps over the lazy dog.",
			salt:      "my-top-secret-salt",
			KV:        "12345678901234567890123456789012",
		},
		{
			name:      "case2",
			plaintext: "short text",
			salt:      "my-top-secret-salt",
			KV:        "12345678901234567890123456789012",
		},
	}

	// loop over each test case and
	// 1. encrypt
	// 2. decrypt
	// 3. validate the original and decrypted text
	for _, tc := range testcases {
		encrypted, err := Encrypt(tc.plaintext, tc.salt, tc.KV)
		if err != nil {
			t.Errorf("%s: encryption returned an error: %v", tc.name, err)
		}

		newplain, err := Decrypt(encrypted, tc.salt, tc.KV)
		if err != nil {
			t.Errorf("%s: decryption returned an error: %v", tc.name, err)
		}

		if newplain != tc.plaintext {
			t.Errorf("%s: returned decrypted plaintext is different: %v", tc.name, newplain)
		}
	}
}
