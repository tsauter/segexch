FROM golang:latest

ENV SRC_DIR=/go/src/gitlab.com/tsauter/segexch/
WORKDIR /app

RUN go version

RUN curl -L -O https://github.com/prometheus/promu/releases/download/v0.15.0/promu-0.15.0.linux-amd64.tar.gz && tar xzf promu-0.15.0.linux-amd64.tar.gz

ADD . $SRC_DIR
RUN cd $SRC_DIR; /app/promu-0.15.0.linux-amd64/promu build; cp segexch /app/
RUN cp -r $SRC_DIR/views /app

EXPOSE 8080

VOLUME /etc/segexch
VOLUME /app/views

ENTRYPOINT ["./segexch"]
