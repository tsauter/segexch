﻿$defaultSegExchURL = "http://localhost:8080/segexch/api/v1/secret"

<#
    .SYNOPSIS
        Fetch a secret from the API of the OTS system.

    .Parameter Magic
        The unique magic string of the requested secret.

    .Parameter Password
        The password for the requested secret.

    .Parameter CopyToClipboard
        If set to true, the password will automaticall send to the Windows clipboard. (Cleartext!)

    .Parameter Url
        The full URL to the API endpoint.

    .Example
        Get-OTSSecret -Magic "magicstring" -Password "12345"

#>
Function Get-OTSSecret {
    Param (
        [parameter(Mandatory=$true)][String]$Magic,
        [parameter(Mandatory=$true)][String]$Password,
        [Switch]$CopyToClipboard=$false,
        [String]$Url=$defaultSegExchURL
    )

    # build data from $Magic and $Password parameter
    $data = @{"Magic" = $Magic; "Password" = $Password }
    Write-Verbose ("Sending data: {0}" -f $Text)

    # Do the web request with the data as HTTP GET parameters
    # If the HTTP response code is not 200 (OK) we assume an error.
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $result = (Invoke-WebRequest -Uri $Url -ContentType "application/json" -Body $data -Method Get -ErrorAction Stop)
    if($result.StatusCode -ne 200) {
        Write-Error ("Web request failed: {0}" -f $result.StatusDescription)
        break
    }

    # Convert the returned body to JSON
    $jsonResult = ($result.Content | ConvertFrom-Json -ErrorAction Stop)

    # Send the content of the URL to the Windows clipboard.
    if ($CopyToClipboard -eq $true) {
        $jsonResult.URL | Set-Clipboard -ErrorAction Stop
        Write-Verbose "URL has been posted to clipboard"
    }

    return $jsonResult
}


<#
    .SYNOPSIS
        Send a text through the API to the OTS system.

    .Parameter APIKey
        The API key, used to authenticate on the OTS system.

    .Parameter Text
        The text itself. HTML will converted to a plain text.

    .Parameter CopyToClipboard
        If set to true, the password will automaticall send to the Windows clipboard.

    .Parameter Url
        The full URL to the API endpoint.

    .Parameter PassThrough
        Returns the generated secret exchange data.

    .Example
        Send-OTSSecret -APIKey "authkey" -Text "my secret to share"

    .Example
        "my secret to share" | Send-OTSSecret -APIKey "authkey" -CopyToClipboard:$true

#>
Function Send-OTSSecret {
    Param (
        [parameter(Mandatory=$true)][String]$APIKey,
        [parameter(Mandatory=$true,ValueFromPipeline=$true)][String]$Text,
        [Switch]$CopyToClipboard=$false,
        [String]$Url=$defaultSegExchURL,
        [Switch]$PassThrough=$false
    )

    # build json data from $APIKey and $Text parameter
    $data = @{"APIKey" = $APIKey; "Secret" = $Text.Trim() }
    Write-Verbose ("Sending data: {0}" -f $Text)

    # Convert the data to JSON, do the web request with the piped json source data.
    # If the HTTP response code is not 200 (OK) we assume an error.
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    $result = (ConvertTo-Json -ErrorAction Stop $data | Invoke-WebRequest -Uri $Url -Method Post -ContentType "application/json" -ErrorAction Stop)
    if($result.StatusCode -ne 200) {
        Write-Error ("Web request failed: {0}" -f $result.StatusDescription)
        break
    }

    # Convert the returned body to JSON
    $jsonResult = ($result.Content | ConvertFrom-Json -ErrorAction Stop)

    # Send the content of the URL to the Windows clipboard.
    if ($CopyToClipboard -eq $true) {
        $jsonResult.URL | Set-Clipboard -ErrorAction Stop
        Write-Verbose "URL has been posted to clipboard"
    }

	# pass the returned data back to powershell pipe
	if ($PassThrough -eq $true) {
		return $jsonResult
	}
    else {
        return
    }
}

# SIG # Begin signature block
# MIIHgQYJKoZIhvcNAQcCoIIHcjCCB24CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUG/A2gcD71qGrFfmCIkTeU6hu
# PlegggUSMIIFDjCCBHegAwIBAgITXAAAhRgpz0eQ0uAHQgAKAACFGDANBgkqhkiG
# 9w0BAQsFADCBoDEgMB4GCSqGSIb3DQEJARYRYWRtaW5Admlhc3RvcmUuZGUxCzAJ
# BgNVBAYTAkRFMRswGQYDVQQIExJCYWRlbi1XdWVydHRlbWJlcmcxEjAQBgNVBAcT
# CVN0dXR0Z2FydDEeMBwGA1UEChMVdmlhc3RvcmUgc3lzdGVtcyBHbWJIMR4wHAYD
# VQQDExV2aWFzdG9yZSBzeXN0ZW1zIEdtYkgwHhcNMTgwMzIxMjIyNDE3WhcNMTkw
# MzIxMjIyNDE3WjB+MRIwEAYKCZImiZPyLGQBGRYCZGUxGDAWBgoJkiaJk/IsZAEZ
# Fgh2aWFzdG9yZTEUMBIGA1UECxMLQWJ0ZWlsdW5nZW4xDTALBgNVBAsTBHZzU1cx
# DjAMBgNVBAsTBVVzZXJzMRkwFwYDVQQDExBTYXV0ZXIsIFRob3JzdGVuMIGfMA0G
# CSqGSIb3DQEBAQUAA4GNADCBiQKBgQC0/wPq4v8RGI4r5QDJy0VeWpdJpZrfwbUK
# RYSB9a+f+uD6HeYsrVOYZJdTUrgsFRZBMM5buSHDpayw/YQdu8y51iYh3LgkTjvx
# Z415gTZWlF+nCFmimQ2Eh1zwSrOoGMZbD0OW+SXrC3KDDCJajnuHauo6AjgW0TEb
# NmoztNfjHQIDAQABo4ICZTCCAmEwJQYJKwYBBAGCNxQCBBgeFgBDAG8AZABlAFMA
# aQBnAG4AaQBuAGcwEwYDVR0lBAwwCgYIKwYBBQUHAwMwCwYDVR0PBAQDAgeAMCsG
# A1UdEQQkMCKgIAYKKwYBBAGCNxQCA6ASDBBzYXJAdmlhc3RvcmUuY29tMB0GA1Ud
# DgQWBBQHHNkUArXqXIu6JAe0Lr7JiPZoFDAfBgNVHSMEGDAWgBSCejGKEB+vSSqw
# GmcfYQ/RQKlfOzCB2gYDVR0fBIHSMIHPMIHMoIHJoIHGhoHDbGRhcDovLy9DTj12
# aWFzdG9yZSUyMHN5c3RlbXMlMjBHbWJIKDEwKSxDTj1udDVkYzEsQ049Q0RQLENO
# PVB1YmxpYyUyMEtleSUyMFNlcnZpY2VzLENOPVNlcnZpY2VzLENOPUNvbmZpZ3Vy
# YXRpb24sREM9dmlhc3RvcmUsREM9ZGU/Y2VydGlmaWNhdGVSZXZvY2F0aW9uTGlz
# dD9iYXNlP29iamVjdENsYXNzPWNSTERpc3RyaWJ1dGlvblBvaW50MIHLBggrBgEF
# BQcBAQSBvjCBuzCBuAYIKwYBBQUHMAKGgatsZGFwOi8vL0NOPXZpYXN0b3JlJTIw
# c3lzdGVtcyUyMEdtYkgsQ049QUlBLENOPVB1YmxpYyUyMEtleSUyMFNlcnZpY2Vz
# LENOPVNlcnZpY2VzLENOPUNvbmZpZ3VyYXRpb24sREM9dmlhc3RvcmUsREM9ZGU/
# Y0FDZXJ0aWZpY2F0ZT9iYXNlP29iamVjdENsYXNzPWNlcnRpZmljYXRpb25BdXRo
# b3JpdHkwDQYJKoZIhvcNAQELBQADgYEA03dtZb8lKfNdebDSgc0hIjClAWSQ+QSW
# CgYEQokc6lPcgRQ3NAvAs2bKh+fXDeUP1nYlZgOMYtgAq/bF5Rw0XdO5McIEF+ST
# u7YWZ9XvslrJNmEFAEBj04uyX+kZXePZBQlE3dZBHNEVhcu+EVrhDl3gaoPKKvf0
# T7ZrSje4O20xggHZMIIB1QIBATCBuDCBoDEgMB4GCSqGSIb3DQEJARYRYWRtaW5A
# dmlhc3RvcmUuZGUxCzAJBgNVBAYTAkRFMRswGQYDVQQIExJCYWRlbi1XdWVydHRl
# bWJlcmcxEjAQBgNVBAcTCVN0dXR0Z2FydDEeMBwGA1UEChMVdmlhc3RvcmUgc3lz
# dGVtcyBHbWJIMR4wHAYDVQQDExV2aWFzdG9yZSBzeXN0ZW1zIEdtYkgCE1wAAIUY
# Kc9HkNLgB0IACgAAhRgwCQYFKw4DAhoFAKB4MBgGCisGAQQBgjcCAQwxCjAIoAKA
# AKECgAAwGQYJKoZIhvcNAQkDMQwGCisGAQQBgjcCAQQwHAYKKwYBBAGCNwIBCzEO
# MAwGCisGAQQBgjcCARUwIwYJKoZIhvcNAQkEMRYEFG8ab+Jkf2vTZ9h700o592jb
# kBYXMA0GCSqGSIb3DQEBAQUABIGAT/J3V/+DZEeXp853lGaZ+qjKyHxZ1xcjNu0O
# YnOt5z0AA8kGm/USM98qOv+GD9jiZe7+bOvPWOEsivLiwfJV1FJ8nCAAIBf9L8Bn
# 8sW/s+5rsdy8rvEluO/Pu0LCPbQHpPZM/azjBV4il2JQMqoY6+pE/GfFYZPGGakU
# P8fFLn4=
# SIG # End signature block
