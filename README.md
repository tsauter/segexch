README
======

Database Setup
--------------
The database will be created automatically during first start.
Please make sure, that the user has enough permissions to manage tables and data in the database.

Prepare the MySQL Server:
1. Create a new empty database
   > create database ots;
2. Create a new user and grand permissions to the database:
   > grant all privileges on ots.* to 'ots_rw'@'localhost' identified by 'ots';
3. Update database privileges
   > flush privileges;

