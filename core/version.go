package core

import (
	"fmt"
)

var (
	// Version is the program version and will
	// be set during promu builds.
	Version = "0"
	// Revision is the GIT version
	Revision = "HEAD"
	// Branch is the GIT branch
	Branch = "MASTER"
	// BuildUser will be set during promu builds
	BuildUser = "xxx@xxx"
	// BuildDate will be set during promu builds
	BuildDate = "just now"
)

// GetVersion returns a string with version information.
// The details are filled during compilation with promu.
func GetVersion() string {
	return fmt.Sprintf("v%s", Version)
}

// GetFullVersion returns a string with version information and timestamp of build
// The details are filled during compilation with promu.
func GetFullVersion() string {
	return fmt.Sprintf("v%s, build date %s", Version, BuildDate)
}
