package core

import (
	"fmt"
	"testing"
)

// TestGetVersion verifies the return of the GetVersion
// function.
func TestGetVersion(t *testing.T) {
	// overwrite the versions
	Version = "99"
	Revision = "TEST-REV"
	Branch = "TEST"
	BuildUser = "yyy@yyy"
	BuildDate = "mydate"

	expected := fmt.Sprintf("v%s", Version)

	ver := GetVersion()
	if ver != expected {
		t.Errorf("version is different: %s (%s)", ver, expected)
	}
}

// TestGetFullVersion verifies the return of the GetFullVersion
// function.
func TestGetFullVersion(t *testing.T) {
	// overwrite the versions
	Version = "99"
	Revision = "TEST-REV"
	Branch = "TEST"
	BuildUser = "yyy@yyy"
	BuildDate = "mydate"

	expected := fmt.Sprintf("v%s, build date %s", Version, BuildDate)

	ver := GetFullVersion()
	if ver != expected {
		t.Errorf("version is different: %s (%s)", ver, expected)
	}
}
