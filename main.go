package main

import (
	"context"
	"crypto/rand"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/csrf"
	"github.com/gorilla/mux"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"github.com/ulule/limiter/v3"
	"github.com/ulule/limiter/v3/drivers/middleware/stdlib"
	"github.com/ulule/limiter/v3/drivers/store/memory"
	"github.com/unrolled/secure"

	"gitlab.com/tsauter/segexch/config"
	"gitlab.com/tsauter/segexch/controllers"
	"gitlab.com/tsauter/segexch/core"
	"gitlab.com/tsauter/segexch/models"
)

func main() {
	// read configuration through Viper
	viper.SetConfigName("segexch")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/segexch")
	viper.SetDefault("ListenAddr", "127.0.0.1:8080")
	viper.SetDefault("HTMLLayout", "bootstrap")
	viper.SetDefault("UseCaptcha", true)
	viper.SetDefault("CleanerInterval", 60*5)       // 5 minutes
	viper.SetDefault("MaxSecretLifetime", 60*60*24) // 1 day
	viper.SetDefault("IPRateLimit", "25-H")
	//viper.SetDefault("PrometheusAddr", ":8080")
	viper.SetEnvPrefix("segexch")
	viper.AutomaticEnv()

	// Print startup banner
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("Failed to determine current hostname: %v", err)
	}
	log.Printf("Starting SegExch on %s, %s\n", hostname, core.GetFullVersion())

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}

	var cfg config.Config
	if err := viper.Unmarshal(&cfg); err != nil {
		log.Fatalf("Error reading config file: %v", err)
	}

	// looks like the Unmarshal function doesn't convert map's
	// populate the password schemes
	parsePasswordSchemesFromConfig(&cfg)

	//log.Printf("Config: %#v", cfg)

	if (cfg.EncryptionSalt == "") || (len(cfg.EncryptionSalt) < 10) {
		log.Fatalf("no encryption salt configured")
	}
	if cfg.CleanerInterval <= 60 {
		log.Fatalf("cleaner interval is to short: %d", cfg.CleanerInterval)
	}
	if cfg.MaxSecretLifetime <= 0 {
		log.Fatalf("no maximum secret lifetime configured: %d", cfg.MaxSecretLifetime)
	}

	log.Printf("Max Secret lifetime was set to: %d seconds", cfg.MaxSecretLifetime)
	log.Printf("Cleaning interval was set to: %d seconds", cfg.CleanerInterval)

	ctx := context.Background()

	log.Printf("Connecting to backend database...")
	secretService, err := models.NewSecretService(&cfg)
	if err != nil {
		log.Fatalf("error initializing secret service: %v", err)
	}
	//WARNING:secretService.DestructiveReset() // this will wipe out the MySQL database
	secretService.AutoMigrate()
	secretService.StartCleaner(ctx)
	log.Printf("Connection to backend database successfully established.")

	secretsC := controllers.NewSecrets(cfg, secretService)
	captchaC := controllers.NewCaptchas(cfg)
	passwordgenC, err := controllers.NewPasswordGenerator(cfg)
	if err != nil {
		log.Fatalf("error initializing password generator: %v", err)
	}

	// initialize access rate limit
	var ratemiddle *stdlib.Middleware
	if viper.GetString("IPRateLimit") != "" {
		rate, err := limiter.NewRateFromFormatted(viper.GetString("IPRateLimit"))
		if err != nil {
			log.Fatalf("error initializing rate limit rate: %v", err)
		}
		ratestore := memory.NewStoreWithOptions(limiter.StoreOptions{
			Prefix: "segex",
		})
		ratelimit := limiter.New(ratestore, rate, limiter.WithTrustForwardHeader(true))
		ratemiddle = stdlib.NewMiddleware(ratelimit)
		log.Printf("Initialized IP rate limit: %s", rate.Formatted)
	}

	// initialize the CSRF protection middleware
	// the key is generate uniquely on each application start
	csfr := csrf.Protect(
		generateCSRFKey(),
		csrf.FieldName("csrfField"),
		csrf.Secure(!cfg.AllowInsecureHTTP),
	)

	// create a new Gorilla router
	// register two endpoints that should *NOT* covered by CSFR protection
	// we have to redirect the "/"-page, otherwise the CSFR middleware is not used,
	// and the form field empty.
	r := mux.NewRouter()
	r.HandleFunc("/", secretsC.RedirectStartpage).Methods("GET")
	r.HandleFunc("/segexch/api/v1/secret", secretsC.APIRead).Methods("GET")
	r.HandleFunc("/segexch/api/v1/secret", secretsC.APICreate).Methods("POST")
	r.HandleFunc("/passwordgen/api/v1/password", passwordgenC.APICreate).Methods("POST")
	r.Handle("/captcha/{CaptchaId}.png", captchaC.CaptchaServer())

	// create a mux subroute, all sites under this page *are* covered by CSFR
	s := r.PathPrefix("/secret").Subrouter()
	s.HandleFunc("/", secretsC.Index).Methods("GET")
	s.HandleFunc("/", secretsC.Create).Methods("POST")
	s.HandleFunc("/{magic:[A-Za-z0-9_-]+}", secretsC.Landing).Methods("GET")
	s.HandleFunc("/{magic:[A-Za-z0-9_-]+}/get", secretsC.Detail).Methods("GET")
	if ratemiddle != nil {
		s.Use(ratemiddle.Handler)
	}
	s.Use(csfr)
	p := r.PathPrefix("/passwordgen").Subrouter()
	p.HandleFunc("/", passwordgenC.Index).Methods("GET")
	p.HandleFunc("/", passwordgenC.WebAPICreate).Methods("POST")
	if ratemiddle != nil {
		p.Use(ratemiddle.Handler)
	}
	p.Use(csfr)

	if cfg.AllowInsecureHTTP {
		log.Printf("*** WARNING: unencrypted HTTP configuration")
	}
	secureMiddleware := secure.New(
		secure.Options{
			SSLRedirect: !cfg.AllowInsecureHTTP,
		},
	)

	log.Printf("Starting segexch %s", core.GetVersion())
	log.Printf("Listening on %s (%s)...", cfg.ListenAddr, cfg.BaseURL)
	http.ListenAndServe(cfg.ListenAddr, secureMiddleware.Handler(r))
}

// generateCSRFKey generates a random 32bit key.
// This key can bey used for CSRF for example
func generateCSRFKey() []byte {
	key := make([]byte, 32)
	_, err := rand.Read(key)
	if err != nil {
		panic(err)
	}
	return key
}

// parsePasswordSchemesFromConfig loads the password schemes from loaded
// viper config and store in Config{}.
// Viper does not populate sub dicts.
func parsePasswordSchemesFromConfig(cfg *config.Config) error {
	schemeList := config.PasswordSchemeList{}

	schemeEntries := viper.GetStringMap("PasswordScheme")

	for name, scheme := range schemeEntries {
		var parsed config.PasswordScheme
		err := mapstructure.Decode(scheme, &parsed)
		if err != nil {
			return fmt.Errorf("failed to parse password scheme: %s: %s", name, scheme)
		}

		//fmt.Printf("%s: %#v\n", name, parsed)
		schemeList[name] = parsed
	}

	cfg.PasswordSchemes = schemeList
	return nil
}
