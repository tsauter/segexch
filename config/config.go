package config

import (
	"fmt"
)

// Config contains the settings for all parts
// of the program.
type Config struct {
	// AllowInsecureHTTP is required to allow CSRF tokens through HTTP
	AllowInsecureHTTP bool `yaml:"AllowInsecureHTTP"`
	// HTTP server configuration
	ListenAddr  string `yaml:"ListenAddr"`
	IPRateLimit string `yaml:"IPRateLimit"`
	BaseURL     string `yaml:"BaseURL"`
	HTMLLayout  string `yaml:"HTMLLayout"`
	UseCaptcha  bool   `yaml:"UseCaptcha"`
	// API and encryption
	APIKey         string `yaml:"APIKey"`
	EncryptionSalt string `yaml:"EncryptionSalt"`
	// Lifetime settings
	MaxSecretLifetime int `yaml:"MaxSecretLifetime"`
	CleanerInterval   int `yaml:"CleanerInterval"`
	// PasswordGenerator settings
	PasswordSchemes        PasswordSchemeList `yaml:"PasswordScheme"`
	PasswordSchemeOrdering []string           `yaml:"PasswordSchemeOrdering"`
	// Database configuration
	DBType    string `yaml:"DBType"`
	DBHost    string `yaml:"DBHost"`
	DBPort    int    `yaml:"DBPort"`
	DBUser    string `yaml:"DBUser"`
	DBPasswd  string `yaml:"DBPasswd"`
	DBName    string `yaml:"DBName"`
	DBVerbose bool   `yaml:"DBVerbose"`
}

// PasswordSchemeList stores all availabe password schemes
type PasswordSchemeList map[string]PasswordScheme

// Get returns the password scheme out of the list.
// It is the keyname and not the desciption of the scheme.
func (psl *PasswordSchemeList) Get(name string) (*PasswordScheme, error) {
	scheme, ok := (*psl)[name]
	if !ok {
		return nil, fmt.Errorf("scheme not found")
	}
	return &scheme, nil
}

// PasswordScheme represents the settings of a various password
// scheme: how should the password looks like?
type PasswordScheme struct {
	Description    string `yaml:"description"`
	Length         int    `yaml:"length"`
	NumDigits      int    `yaml:"NumDigits"`
	NumSymbols     int    `yaml:"NumSymbols"`
	NoUpper        bool   `yaml:"NoUpper"`
	AllowRepeat    bool   `yaml:"AllowRepeat"`
	AllowedSymbols string `yaml:"AllowedSymbols"`
}
