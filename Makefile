imagename = registry.gitlab.com/tsauter/segexch
version = $$(cat VERSION)

all: fmt test vet lint
	make -C config all
	make -C controllers all
	make -C core all
	make -C models all
	make -C views all

fmt:
	go fmt

test:
	go test

vet:
	go vet

lint:
	golint

docker-build:
	@echo "***"
	@echo "*** Version: $$(cat .promu.yml | grep version)"
	@echo "***"
	@echo "***"
	docker build -t $(imagename):$(version) -t $(imagename):latest .
	@echo "***"
	@echo "*** Version: $$(cat .promu.yml | grep version)"

docker-push:
	# docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
	docker push $(imagename):$(version)
	docker push $(imagename):latest

