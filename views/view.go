package views

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"

	"gitlab.com/tsauter/segexch/core"
)

var (
	// LayoutDir represents the directory of the basic html template files.
	LayoutDir = "views/_layouts/"
	// TemplateDir is the toplevel directory of all html templates.
	TemplateDir = "views/"
	// TemplateExt specifies the extensions of the go html template files
	TemplateExt = ".gohtml"
)

// NewView creates a new Views for the page, based on the specified layout.
// To do this, each entry in the files slices will be prepended by the TemplateDir
// directory. After that, each entry will be extended by the TemplateExt (the extension).
// Finally, all files from the base layout will be appended to the slice.
// e.g.
//   - 1: secrets/index
//   - 2: views/secrets/index
//   - 3: views/secrets/index.gohtml
//   - 4: views/secrets/index.gohtml views\_layouts\alert.gohtml views\_layouts\bootstrap.gohtml views\_layouts\errors.gohtml views\_layouts\footer.gohtml views\_layouts\navbar.gohtml
//
// When all files are found, the Go template engine loads and parse all of these files from
// the filesystem.
func NewView(layout string, files ...string) *View {
	addTemplatePath(files)
	addTemplateExt(files)
	files = append(files, layoutFiles(LayoutDir, layout)...)
	t, err := template.ParseFiles(files...)
	if err != nil {
		// we are going to panic during the program start
		panic(err)
	}

	return &View{
		Template: t,
		Layout:   layout,
	}
}

// A View represents a preloaded Go html template for one single page (index, detail, ...).
// The Layout specifies the to be used base template (bootstrap, ...).
// The Template contains the already loaded and parsed files (from local filesystem).
//
// Nested structure of the templates:
//
//	 _layouts/bootstrap.gohtml
//	  {{define "layout"}}                    <-- the name of our BASE layout
//	    {{template "navbar"}}                <-- additional files from the base layout
//	    {{range .Alerts}}                    <-- loop of possible alerts and display them
//	        {{template "alert" .}}           <-- error file from base layout to display errors
//	    {{end}}
//	    {{template "yield" .Yield}}          <-- here comes the template for our view (with data from .Yield)
//	  {{end}}
//	 _layouts/navbar.gohtml
//	  {{define "navbar"}}
//	    {{template "navbar"}}
//	  {{end}}
//	_layouts/...
//	secrets/detail.gohtml
//	  {{define "yield"}}                     <-- our page content, data accessible trough "."
//	     {{.Text}}
//	  {{end}}
type View struct {
	// Template are the loaded and parsed files.
	Template *template.Template
	// Layout is the name of the base template.
	Layout string
}

// Render creates the HTML content of the page and send them
// to the http.ResponseWriter. This includes html headers
// and the final html code itself. Variables are already replaced.
// In case of an error we try send a useful error message.
// However, this depends on the state when the error happend.
func (v *View) Render(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "text/html")

	// extend the data with global site details
	var err error
	switch data.(type) {
	case Data:
		newData := data.(Data)
		siteData := SiteData{
			Version: core.GetVersion(),
		}
		newData.WebsiteData = siteData
		err = v.Template.ExecuteTemplate(w, v.Layout, newData)
	default:
		err = v.Template.ExecuteTemplate(w, v.Layout, data)

	}

	if err != nil {
		log.Println(err)
		fmt.Fprintf(w, "<h1>Something went wrong</h1>"+
			"<p>Email us if the problem perists.</p>")
	}
}

// RenderError creates an HTTP error page. This contains the HTTP
// error status code and the html code of the original page
// extended by an error message. We use the Render() function.
// The used html have to take care, that the error message is displayed
// properly.
func (v *View) RenderError(w http.ResponseWriter, data interface{}) {
	w.WriteHeader(http.StatusInternalServerError)
	v.Render(w, data)
}

// FIXME: is this function really needed?
//func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//v.Render(w, nil)
//}

// addTemplatePath takes the files slice and prepend the TemplateDir
// directory in front of each entry.
// No validation of the existance of the file.
// e.g.
//
//	secrets/index -> views/secrets/index
func addTemplatePath(files []string) {
	for i, f := range files {
		files[i] = TemplateDir + f
	}
}

// addTemplateExt takes the files slice and append the TemplateExt
// file extension at the end of each entry.
// No validation of the existance of the file.
// e.g.
//
//	views/secrets/index -> views/secrets/index.gohtml
func addTemplateExt(files []string) {
	for i, f := range files {
		files[i] = f + TemplateExt
	}
}

// layoutFiles globs the local file system for all available template files (extension)
// and return them.
// The directory is combined of the following parts: baseDir (aka LayoutDir) + OurLayoutName
// The glob pattern is: *.TemplateExt
func layoutFiles(baseDir string, baseLayoutName string) []string {
	// make a valid clean path on Windows and Linux
	// TODO: is this secure? possible loading of other files?
	searchDir := filepath.FromSlash(path.Clean(path.Join(baseDir, baseLayoutName))) + string(os.PathSeparator)
	files, err := filepath.Glob(searchDir + "*" + TemplateExt)
	if err != nil {
		panic(err)
	}
	return files
}
