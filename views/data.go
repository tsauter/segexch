package views

const (
	// LevelError represents an error state
	LevelError alertLevel = "danger"
	// LevelWarning represents a warning state
	LevelWarning alertLevel = "warning"
	// LevelInfo represents an info state
	LevelInfo alertLevel = "info"
	// LevelSuccess represents a success state
	LevelSuccess alertLevel = "success"
)

// AlertGeneric is a generic Alert that include a generic error message.
var AlertGeneric = AlertError("Something went wrong. Please try again, and contact us if the problem persists.")

// AlertMagicMissing is the alert if a magic is missing.
var AlertMagicMissing = AlertError("Oops! Not sure what you're looking for...")

type alertLevel string

// An Alert represets an alert message.
// The message contains an error level and an error message.
type Alert struct {
	Level   alertLevel
	Message string
}

// AlertError creates an error alert with the given message.
func AlertError(msg string) Alert {
	return Alert{LevelError, msg}
}

// AlertWarning creates a warning alert with the given message.
func AlertWarning(msg string) Alert {
	return Alert{LevelWarning, msg}
}

// AlertInfo creates an info alert with the given message.
func AlertInfo(msg string) Alert {
	return Alert{LevelInfo, msg}
}

// AlertSuccess creates a success alert with the given message.
func AlertSuccess(msg string) Alert {
	return Alert{LevelSuccess, msg}
}

// SiteData contains usefull globally available details of the website
// The content is per site and not per page
type SiteData struct {
	Version string
}

// A Data type always used to pass data to the rendering functions.
// Error pages shows Alerts, all other pages shows the Yield property.
type Data struct {
	Alerts      []Alert
	WebsiteData SiteData
	Yield       interface{}
}

// AddAlerts appends the given alerts to the Data struct.
func (d *Data) AddAlerts(alerts ...Alert) {
	d.Alerts = append(d.Alerts, alerts...)
}
