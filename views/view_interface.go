package views

import (
	"net/http"
)

// A ViewRenderer specifies the functions which are
// needed to build outputs for the frontend (MVC model).
type ViewRenderer interface {
	Render(w http.ResponseWriter, data interface{})
	RenderError(w http.ResponseWriter, data interface{})
}
