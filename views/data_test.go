package views

import (
	"fmt"
	"testing"
)

// TestAlertError tests the AlertError() function.
func TestAlertError(t *testing.T) {
	a := AlertError("test")
	str := fmt.Sprintf("%#v", a)
	if str != `views.Alert{Level:"danger", Message:"test"}` {
		t.Errorf("different data returned: %s", str)
	}
}

// TestAlertWarning tests the AlertWarning() function.
func TestAlertWarning(t *testing.T) {
	a := AlertWarning("test")
	str := fmt.Sprintf("%#v", a)
	if str != `views.Alert{Level:"warning", Message:"test"}` {
		t.Errorf("different data returned: %s", str)
	}
}

// TestAlertInfo tests the AlertInfo() function.
func TestAlertInfo(t *testing.T) {
	a := AlertInfo("test")
	str := fmt.Sprintf("%#v", a)
	if str != `views.Alert{Level:"info", Message:"test"}` {
		t.Errorf("different data returned: %s", str)
	}
}

// TestAlertSuccess tests the AlertSuccess() function.
func TestAlertSuccess(t *testing.T) {
	a := AlertSuccess("test")
	str := fmt.Sprintf("%#v", a)
	if str != `views.Alert{Level:"success", Message:"test"}` {
		t.Errorf("different data returned: %s", str)
	}
}

// TestAddAlerts tests the AddAlerts() function.
func TestAddAlerts(t *testing.T) {
	var d Data
	d.AddAlerts(AlertError("Test"), AlertSuccess("Test"))

	exp := `views.Data{Alerts:[]views.Alert{views.Alert{Level:"danger", Message:"Test"}, views.Alert{Level:"success", Message:"Test"}}, Yield:interface {}(nil)}`

	str := fmt.Sprintf("%#v", d)
	if str != exp {
		t.Errorf("different data returned: %s", str)
	}
}
