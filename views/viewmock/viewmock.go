package viewmock

import (
	"net/http"
	"text/template"
)

// A ViewMock is a mocker to emulate a View
type ViewMock struct {
	TemplateString string
}

// Render renders a text template and includes the Yield-data of the passed in struct.
func (vm *ViewMock) Render(w http.ResponseWriter, data interface{}) {
	t, err := template.New("Render").Parse(vm.TemplateString)
	if err != nil {
		return
	}
	t.Execute(w, data)
}

// RenderError renders a text template and includes the Alerts-data of the passed in struct.
func (vm *ViewMock) RenderError(w http.ResponseWriter, data interface{}) {
	t, err := template.New("RenderError").Parse("{{range .Alerts}}{{.Level}}/{{.Message}};{{end}}")
	if err != nil {
		return
	}
	t.Execute(w, data)
}
