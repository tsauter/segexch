package views

import (
	"html/template"
	"io/ioutil"
	"net/http/httptest"
	"os"
	"reflect"
	"testing"
)

// TestRender tests the Render() function
func TestRender(t *testing.T) {
	testcases := []struct {
		name              string
		data              Data
		statuscode        int
		contentTypeHeader string
		expected          string
	}{
		{
			name:              "case1",
			data:              Data{Yield: ""},
			statuscode:        200,
			contentTypeHeader: "text/html",
			expected:          "",
		},
		{
			name:              "case2",
			data:              Data{Yield: "testmessage"},
			statuscode:        200,
			contentTypeHeader: "text/html",
			expected:          "<strong>testmessage</strong>",
		},
	}

	layout := "test"

	tmpl, err := template.New(layout).Parse(`{{define "test"}}{{with .Yield}}<strong>{{.}}</strong>{{end}}{{end}}`)
	if err != nil {
		t.Fatalf("failed to create template: %v", err)
	}

	v := View{
		Template: tmpl,
		Layout:   layout,
	}

	for _, tc := range testcases {
		// loop over each test case and validate the result
		// in this case, the sendJSON takes a structure, convert it to JSON
		// and send everything to an http.ResponseWriter.
		// Result code, Content-type and body content have to match.
		w := httptest.NewRecorder()
		v.Render(w, &tc.data)
		resp := w.Result()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("failed to read body")
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statuscode {
			t.Errorf("%s: expected statuscode is different: %d", tc.name, resp.StatusCode)
		}

		if resp.Header.Get("Content-Type") != tc.contentTypeHeader {
			t.Errorf("%s: expected content-type header is different: %s", tc.name, resp.Header.Get("Content-Type"))
		}

		if string(body) != tc.expected {
			t.Errorf("%s: expected body is different: %s", tc.name, body)
		}
	}
}

// TestRenderError tests the RenderError() function
func TestRenderError(t *testing.T) {
	testcases := []struct {
		name       string
		data       Data
		statuscode int
		expected   string
	}{
		{
			name:       "case1",
			data:       Data{Yield: ""},
			statuscode: 500,
			expected:   "",
		},
		{
			name:       "case2",
			data:       Data{Alerts: []Alert{AlertError("testmessage")}},
			statuscode: 500,
			expected:   "<strong>danger/testmessage</strong>",
		},
	}

	layout := "test"

	tmpl, err := template.New(layout).Parse(`{{define "test"}}{{range .Alerts}}<strong>{{.Level}}/{{.Message}}</strong>{{end}}{{end}}`)
	if err != nil {
		t.Fatalf("failed to create template: %v", err)
	}

	v := View{
		Template: tmpl,
		Layout:   layout,
	}

	for _, tc := range testcases {
		// loop over each test case and validate the result
		// in this case, the sendJSON takes a structure, convert it to JSON
		// and send everything to an http.ResponseWriter.
		// Result code, Content-type and body content have to match.
		w := httptest.NewRecorder()
		v.RenderError(w, &tc.data)
		resp := w.Result()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatalf("failed to read body")
		}
		resp.Body.Close()

		if resp.StatusCode != tc.statuscode {
			t.Errorf("%s: expected statuscode is different: %d", tc.name, resp.StatusCode)
		}

		if string(body) != tc.expected {
			t.Errorf("%s: expected body is different: %s", tc.name, body)
		}
	}
}

/*
// FIXME: is this function really needed?
func TestServeHTTP(t *testing.T) {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}
*/

// TestAddTemplatePath tests the addTemplatePath() function.
func TestAddTemplatePath(t *testing.T) {
	filesIn := []string{"a", "b", "c"}
	filesOut := []string{TemplateDir + "a", TemplateDir + "b", TemplateDir + "c"}

	addTemplatePath(filesIn)

	if !reflect.DeepEqual(filesIn, filesOut) {
		t.Errorf("generated slice is different: %v", filesIn)
	}

}

// TestAddTemplateExt tests the addTemplateExt() function.
func TestAddTemplateExt(t *testing.T) {
	filesIn := []string{"a", "b", "c"}
	filesOut := []string{"a" + TemplateExt, "b" + TemplateExt, "c" + TemplateExt}

	addTemplateExt(filesIn)

	if !reflect.DeepEqual(filesIn, filesOut) {
		t.Errorf("generated slice is different: %v", filesIn)
	}

}

// TestLayoutFiles tests the layoutFiles() function.
func TestLayoutFiles(t *testing.T) {
	baseDir := "_layouts"
	layout := "bootstrap"
	files := []string{"alert.gohtml", "bootstrap.gohtml", "errors.gohtml", "footer.gohtml", "navbar.gohtml"}

	// build the list of expexted files; this isn't hardcoded because of the different separator on Unix/Windows
	var filesExpected []string
	for _, f := range files {
		filesExpected = append(filesExpected, baseDir+string(os.PathSeparator)+layout+string(os.PathSeparator)+f)
	}

	filesOut := layoutFiles(baseDir, layout)

	if !reflect.DeepEqual(filesOut, filesExpected) {
		t.Errorf("generated slice is different: %v", filesOut)
	}

}
